# PLMdetect tool
PLMdetect is described in the following articles :
+ Improved detection of motifs with preferential location in promoters V. BERNARD, A. LECHARNY, and V. BRUNAUD. Genome 2010
+ TC-motifs at the TATA-box expected position in plant genes: a novel class of motifs involved in the transcription regulation. V. BERNARD, V. BRUNAUD and A. LECHARNY. BMC Genomics 2010

The main goal is to detect over-represented motifs at a specific distance from the TSS or TTS in DNA sequences (gene-proximal regions).

## Description
PLMdetect has been developed since 2010. A second version was released in 2019 with new options (enrichment, parallelization).

## Languages
+ Perl (v5.22)
+ R (v3.3.3)

## OS
+ Linux

## Getting Started
The following files or repositories are mandatory :
+ PLMdetect.pl
+ PLMdetect_threads.pl
+ LIB_PERL_R

## Prerequisites
The following Perl libraries are needed :
+ Parallel
+ Getopt
+ File
+ DBI

## Running

To run PLMdetect with all default parameter use the following command :
```
perl PLMdetect.pl --seqFile sequence_file.fasta --motifFile motif_file.txt --dir Result_repository_name
```
+ *seqFile*: gives the sequence file with sequences aligned on a reference (Transcription Start Site or Transcription Terminaison Site). **The name of the sequence file has to finish by the extension .fa or .fasta.**
+ *motifFile*: gives all the motifs that are tested against the sequences. This file needs to respect the following format.
```
TATAWA
CTCCCT
TATA
GAATCTCTG
ATCG
```
+ dir: gives the name of the repository which will contain all results. **PLMdetect creates this repository.**

### Options
+ *alpha*: The level risk, (1-alpha) is the confidence level (default : 0.01)
+ *strand*: One or two-strand studied (default: 2)
+ *scoreIC*: The number of deviance from confidence interval to consider PLM (default: 1)
+ *replaceDir*: y or n - if y and the result directory already exist, delete it and replace it with a new one with the same name. If n and the result directory already exist, rename the new directory by adding "_1" (default: n)
+ *slidingWindow*: Define the size of the sliding window for graphics, an integer between 1 and 100 (default: increasing size)
+ *LA1*: The first boundary of the learning area, corresponding to the first nucleotide coordinate compared to the TSS (default: -1000)
+ *LA2*: The second boundary of the learning area, corresponding to the position of the end of the learning area, corresponding to the first base of the studied area (default: -500)
+ *format*: Graphics format - bmp or pdf (default : pdf)
+ *round*: Boolean (T or F) for rounding functionnal window values (5 bases) (default : F)
+ *UTR*: Choose the region which will be analyzed (5 or 3) (default: 5)
+ *annotation*: Add functionnal annotation to each motif contained in database (y or n) (default : y)
+ *smooth*: Double smoothing for the graphics, when other peak scores are higher than 50% of the maximum peak (y or n) (default y)
+ *publi*: Produces graphics in black and white (T or F) (default: F)
+ *thread*: Number of threads to use (integer) (default 1)
+ *enrichment*: File containing all sequences which are considered as the reference for a hypergeometric test (default: none enrichment)

A classical command looks like :
```
perl PLMdetect.pl --seqFile sequence_file.fasta --motifFile motif_file.txt --dir Result_repository_name --thread 10 - -enrichment reference_sequences.fasta
```

## Results
The result repository contains different files and sub repositories:
+ **Bilan_PLM.csv**: File containing all the information on the PLMs detected (position, functional window, annotation)
    * *Score*: Score given to the motif determined with the high of the peak
    * *Preferential_position*: Position of the maximal peak
    * *Start*: Position of the first base where the distribution is higher than the confidence interval
    * *Stop*: Position the last base where the distribution is higher than the confidence interval.
    * *Functionnal_window_size*: Distance between the start and stop
    * *Number_genes*: Number of genes that contains the PLM between the start and stop in their sequences
    * *Total_number_genes*: Number of genes in the dataset
    * *Percentage_genes*: Number_genes/Total_number_genes
    * *Sliding_window_used*: Size of the window used to count the motifs in sequences
    * *Function*: Function of the motif
    * *Core_motif*: 1 or 0, indicating if the motif is a core motif (minimal functional motif). One is the case.
    * *Family_place*: Indicate the place of the motif in comparison with other motifs; absolute_father is a core motif while a Father_son has a smaller functional motif but a longer one.
    * *Description*: Describe the function of the motif
    * *DOI*: DOI of articles describing the function of the motifs
    * *Organisms*: Organisms in which the motif is already described
    * *Number_genes_REF*: When enrichment is done, give the number of genes in the reference containing the motif
    * *Total_number_genes_REF*: When enrichment is done, give the total number of genes in the reference
    * *p-value*: value obtained with the enrichment test (hypergeometric)
    * *FDR*: pvalue adjusted with Benjamini-Hochberg procedure
    * *Bonferroni*: pvalue adjusted with Bonferroni procedure
    * *PLM_genes.txt*: Matrix, which contains the motifs and the genes. If a gene has the motif in its sequence, the value one is attributed. Else 0 is attributed.
    * *PLM_enrichment.csv*: Summarize the results of the enrichment test
    * *Number_genes_GROUP*: Number of genes that contains the PLM between the start and stop in their sequences
    * *Total_number_genes_GROUP*: Number of genes in the dataset
    * *Percentage_genes_GROUP*: Number_genes_GROUP/Total_number_genes_GROUP
    * *Number_genes_REF*: Gives the number of genes in the reference containing the motif
    * *Total_number_genes_REF*: Gives the total number of genes in the reference
    * *Percentage_genes_REF*: Number_genes_REF/Total_number_genes_REF
    * *PLM_graphs*: Repository with all PLM graphics
    * *PLM_lists*: Repository with lists of genes that have a given PLM
    * *Elimines_Intermediaires*: Contains all motifs which are not PLM (score < 1 or peak in learning region)
    * *MotifsDInteret*: Contains all motifs which present a peak but do not respect all criteria (number of genes too small, sliding window size too big)
