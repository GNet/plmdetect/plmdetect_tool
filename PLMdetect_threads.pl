#!/usr/bin/env perl

use strict;
use Carp;
use Cwd;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre ou est le pgm
my $pgmDir=dirname($0)."/LIB_PERL_R";
require "$pgmDir/CHEMIN.pm";

###################################################################################################################
## fichiers et repertoires donnes par l'utilisateur

# parametres
my $fSeq; # name parameter -seqFile
my $fSeq_doc="-seqFile a file containing the promoters (1000 bases before TSS/TTS and 500 bases after) to study in fasta format\n";
my $fMotif; # name parameter -motifFile
my $fMotif_doc="-motifFile a file containing a list of motifs to study (see also generateMotif.pl)\n";
my $dir; # name parameter -dir
my $dir_doc="-dir a working directory name to output all the results\n";
my $Date = localtime; #date

## parametres du graphe defauts
my $alpha   = $CHEMIN::alphaIC; # risque pour Intervalle de Confiance
my $seuilIC = $CHEMIN::seuilIC; # seuil d'ecart a lintervalle de Confiance (1 fois, 2 fois etc...))


my $brin=$CHEMIN::strand;
my $replaceDir = $CHEMIN::replaceDir; #variable pour remplacer le répertoire portant déjà le nom
my $lissage = $CHEMIN::lissage; #variable indiquant la valeur du lissage (taille de la fenêtre glissante) pour les graphiques (par défaut fenêtre croissante)
my $format = $CHEMIN::form; #variable définissant le format des graphiques en sortie de R
my $arrondi = $CHEMIN::arrondi; #booléen pour arrondir à 5 bases près les fenêtres fonctionnelles
my $UTR = $CHEMIN::UTR; #3 ou 5 correspondant à la région que l'on souhaite étudier
my $BD = $CHEMIN::BD; #y ou n pour indiquer si la recherche des annotations doit se faire dans la BD
my $doublelissage = $CHEMIN::doublelissage; #y ou n pour décider d'appliquer le double lissage ou non
my $publi = $CHEMIN::publi; #T ou F pour indiquer si les figures sont à destinations d'une publication (rendu en noir et blanc)
my $LA1 = $CHEMIN::DEB; #Limite inférieure de la région d'apprentissage qui par défaut est à -1000
my $LA2 = $CHEMIN::DEB + $CHEMIN::APPRENT; #Limite supérieure de la région d'apprentissage, par défaut à -500
my $LimInfPP = $LA2; #Limite inférieure de détection des PP, -500 par défaut

# messages et controles des parametres
my $entete = "\n\tYou don't submit all mandatory parameters\n\t~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
my $entete2 = "\n\tError in optional parameters \n\t~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

###################################################################################################################
GetOptions (
	    "seqFile:s" => \$fSeq,
	    "motifFile:s" => \$fMotif,
	    "dir=s" => \$dir,
	    "alpha:s" => \$alpha,
	    "strand:s" => \$brin,
	    "scoreIC:s"=> \$seuilIC,
	    "replaceDir:s"=>\$replaceDir,
	    "slidingWindow:s"=>\$lissage,
	    "format:s"=>\$format,
	    "round:s"=>\$arrondi,
	    "UTR:s"=>\$UTR,
	    "annotation:s"=>\$BD,
	    "smooth:s"=>\$doublelissage,
	    "publi:s"=>\$publi,
		"LA1:s" => \$LA1,
		"LA2:s" => \$LA2);

my $valParameters="*** Mandatory parameters ***\n \t$fSeq_doc \t$fMotif_doc \t$dir_doc";
my $valoptionDefault="\n*** Optional Parameters default used: ***\n
-alpha   --> $alpha the level of risk, (1-alpha) is the confidence level (default $CHEMIN::alphaIC)
-strand  --> One or Two strands (default 2 strands)
-scoreIC --> $seuilIC the number of deviance from confidence interval to consider PLM (default $CHEMIN::seuilIC)
-replaceDir --> With y if the directory already exists, delete it and replace by a new one with the same name 
		With n if the directory already exists, rename the new directory by adding \"_1\" (default $CHEMIN::replaceDir)
-slidingWindow --> Define the size of the sliding window for graphics, an integer between 1 and 100 (default increasing size)  
-format --> Graphics format : bmp or pdf (default $CHEMIN::form)
-round --> Boolean (T or F) for rounding functionnal window values (5 bases) - default $CHEMIN::arrondi
-UTR --> Choose the region which will be analysed (5 or 3) - default $CHEMIN::UTR
-annotation --> Add functionnal annotation to each motif contained in database (y or n) - default $CHEMIN::BD
-smooth --> Double smoothing for the graphics (when other peak scores are higher than 50% of the maximum peak) (y or n) - default $CHEMIN::doublelissage
-publi --> Produces graphics in black and white (T or F) - default $CHEMIN::publi
-LA1 --> First boudary of the learning area, it corresponds to the first coordinate of the sequences - default $LA1
-LA2 --> Second boudary of the learning area, it corresponds to the first coordinate of the studied region - default $LA2

____________________________________________________________________________\n \n"; #Ligne pour la mise en forme des résultats (sépare les options et le reste des résultats


#########################################################
### les arguments de la ligne de commande sont valides? #
#########################################################

##Vérifications
#le nom doit contenir l'extension .fasta ou .fa
if(!($fSeq =~ m/\.fa$/)&&($fSeq =~ m/\.fasta$/)&&($fSeq =~ /\.fa$/m)&&($fSeq =~ /\.fasta$/m)){
	print "INPUT ERROR : The sequence file needs a .fasta or .fa extension, just rename the file \n\n";
	exit;
}

#le nom du répertoire est bien spécifié
if($dir eq ""){
	print "INPUT ERROR : PLMdetect needs a directory name \n\n";
	exit;
}

#Le paramètre de publication est bien conforme
if($publi ne "T" && $publi ne "F"){
	print "INPUT ERROR : \"publi\" has to be T (true) or F (false) \n\n";
	exit;
}

#Vérifie que le paramètre de fenetre glissante entré est bien conforme (un entier compris entre 1 et 100)
if($lissage ne ""){
	if( ! ( $lissage =~ m/^\d*$/ ) ){
		print "INPUT ERROR : The size of the sliding window has to be an integer \n \n";
		exit;
	}
	if($lissage < 1 || $lissage > 100){
		print "INPUT ERROR : The size of the sliding window has to be between 1 and 100 \n \n";
		exit;	
	}
}

#Vérifie que le paramètre de format de fichier est conforme (pdf ou bmp)
if($format ne "bmp" && $format ne "pdf"){
	print "INPUT ERROR : Format can be only pdf or bmp \n \n";
	exit;
}

#Vérifie que le paramètre round est bien T ou F
if($arrondi ne "T" && $arrondi ne "F"){
	print "INPUT ERROR : Round has to be T (true) or F (false)\n\n";
	exit;
}

#Vérifie que le paramètre UTR est bien 5 ou 3
if($UTR != 5 && $UTR != 3){
	print "INPUT ERROR : UTR has to be 5 (5' UTR) or 3 (3' UTR)\n\n";
	exit;
}

#Vérifie que le annotation est conforme
if($BD ne "y" && $BD ne "n"){
	print "INPUT ERROR : annotation parameter has to be y (yes) or n (no)\n\n";
	exit;
}

#Vérifie que le paramètre smooth est conforme
if($doublelissage ne "y" && $doublelissage ne "n"){
	print "INPUT ERROR : smooth parameter has to be y (yes) or n (no)\n\n";
	exit;
}

#Vérifie que si un double lissage est demandé et le paramètre de lissage a été fixé, le programme renvoie une erreur car contradictoire
if($doublelissage eq "y" && $lissage ne ""){
	print "CONTRADICTORY INPUTS : Double smoothing is not possible when the slinding window size is fixed\n\n";
	exit;
}

$LimInfPP = $LA2;

################################################################################################################

# chemin ou le user lance le programme
my $dirWork=cwd();

#repertoire de travail
$dir=$dirWork."/".$dir;

#création du répertoire de travail si un remplacement est demandé
if($replaceDir eq "y"){
	if(-e $dir){
		system("rm -R ".$dir);
	}

	mkdir($dir) || die "Problem with the directory $dir $!\n";
}

#création du répertoire de travail si un remplacement n'est pas demandé
my $nbDir = 1;
if($replaceDir eq "n"){
	if(-e $dir){
		my $dir_inter = $dir."_".$nbDir;
		while(-e $dir_inter){
			$nbDir = $nbDir+1;
			$dir_inter = $dir."_".$nbDir;			
		}
		$dir = $dir_inter;
	}
	mkdir($dir) || die "Problem with the directory $dir $!\n";
}

# gestion des log
my $filelog=$dir."/PLMdetect.log";
open(LOG,">$filelog")|| die ("$filelog $!\n");;

print LOG "usage :  PLMdetect.pl parameters\n $valParameters $valoptionDefault\n";
print LOG "Repertoire de resultats: $dir";

# gestion des fichiers de sortie ou intermediaires

if($fMotif eq "" || !(-e $fMotif)){
	print "$entete enter a name of file of motifs (parameter motifFile) or $fMotif does not exist\n";
	print LOG "$entete enter a name of file of motifs (parameter motifFile) or $fMotif does not exist\n";
  exit;
}
if($fSeq eq "" || !(-e $fSeq)){
	print "$entete enter a name of file of promoters (parameter seqFile) or $fSeq does not exist\n";
 	print LOG "$entete enter a name of file of promoters (parameter seqFile) or $fSeq does not exist\n";
  exit;
}
## Repertoires de rangement peuvent etre crees
# sous-repertoires de travail
if ($dir eq ""){
  print "$entete enter a name of directory (parameter dir) to input results, the program is in charge to create the directory\n";
  print LOG "$entete enter a name of directory (parameter dir) to input results, the program is in charge to create the directory\n";
  exit;
}


mkdir($dir."/data") || die " Problem with the directory $dir/data $!n";
mkdir($dir."/data/TEMPO/") || die "Problem with the directory $dir/data/TEMPO $!\n";
my $TEMPO = $dir."/data/TEMPO/$$/";
mkdir $TEMPO || die "Problem with the directory $TEMPO $!\n"; ## Repertoire ou seront les fichiers binvalues pour generer les graphes
my $dir=$dir."/";
my $Intermed = "Elimines_Intermediaires/";
my $RepListeMotif = "ListeMotifs/";
my $WC = "WordCounter/";
my $B = "Bilan/";
my $RT = "MotifsDInteret/";
my $dir2 = "$dir$RT";
mkdir "$dir$Intermed" || die "mkdir $dir$Intermed : $!";

###################################################################
##### FICHIER DE MOTIFS #####
###################################################################

mkdir ("$dir$RepListeMotif")|| die "pb $dir$RepListeMotif : $!";; ## Repertoire qui va contenir la liste des motifs
my $Msur = 0;
## On verifie qu'il n'y a bien QUE des motifs
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
open(IN,"$fMotif") || die ("$fMotif: $!\n");
my $fout=$dir.$RepListeMotif."/listM_ok";
open(OUT,">$fout") || die ("$fout pb $!\n");
while(my $M=<IN>){
	$M = uc($M);
  chomp($M);
  $M=~ s/\s//;
	print OUT "$M\n";
	$Msur++;
}

close(OUT);
close(IN);

my $nbSeq;

open SEQUENCES, $fSeq;
while(<SEQUENCES>){
	chomp($_);
	if($_ =~ m/^>/){
		$nbSeq++;
	}
}
close SEQUENCES;

print LOG "\n****************************\n";
print LOG "Motifs : $Msur motifs read \n";
print LOG "Analyze is running...\n";
print LOG "$nbSeq sequences read \n";
###################################################################################################################
######################## ON LANCE LA REPRESENTATION GRAPHIQUE ET LA RECHERCHE DE PICS .... ########################
###################################################################################################################

my $valpg=system(dirname($0)."/LIB_PERL_R/Representations_Graphiques.pl --tfa $fSeq --repL $dir$RepListeMotif --rangement $dir2 --brin $brin --data $TEMPO --LA1 $LA1");
	
if($valpg != 0 ){print "*** Error: $valpg $!***\n";print LOG "*** Error: $valpg $!***\n";}
else{print LOG "-- Graphical pattern OK\n";}

## Recherche de pics
#~~~~~~~~~~~~~~~~~~~
my $ff = "surRepresente/";
my $Res = "$dir2$ff";

$valpg=system(dirname($0)."/LIB_PERL_R/EtudePics.pl  --Res $dir2$ff --alpha $alpha --TFA $fSeq --brin $brin --data $TEMPO --seuilIC $seuilIC --lissage $lissage --format $format --arrondi $arrondi --UTR $UTR --smooth $doublelissage --publi $publi --LA1 $LA1 --LA2 $LA2");
if($valpg != 0 ){print "*** Error : $valpg $!***\n";print LOG "*** Error : $valpg $!***\n";}
else{print LOG "-- Peak Study OK \n";}

# cree les repertoires contenant les donnees genome et sous-set 
mkdir ("$dir$Intermed$ff") || die "mkdir $dir$Intermed$ff : $!";
#################################################################
##################### CE QU'ON dirE DANS LE REPERTOIRE D'INTERET
## les fichiers graphiques



opendir REPF,"$Res$WC";
my @fichi;
if($format eq "bmp"){
	 @fichi = grep /$CHEMIN::plotbmp$/, readdir REPF;

}

elsif($format eq "pdf"){
	 @fichi = grep /$CHEMIN::plotpdf$/, readdir REPF;
}
closedir REPF;

foreach(@fichi){system ("mv  $dir2$ff$WC$_ $dir2$ff$_");}

## les fichier __inclus
opendir REPF,"$dir2$ff$WC";
my @fichi = grep /__inclus$/, readdir REPF;
closedir REPF;
foreach(@fichi){
  system ("mv  $dir2$ff$WC$_ $dir2$ff$_");
}

################################################################
############# CE QU'ON CONSERVE DANS FICHIER COMPTAGE / SANS PIC
## les fichiers WC2gene number
my $desti = "ListeMotifsInteret";
#rmdir ("$dir2$ff$WC$B\WC2geneNumber"); ## Suppression ok s'il y avait rien dedans...
## si suppression HS :
if (-e "$dir2$ff$WC$B\WC2geneNumber" == 1) {
 	 system("mv $dir2$ff$WC$B\WC2geneNumber $dir2$ff$desti");
}
## les representations SANS PIC
opendir REP3,"$dir2$ff$WC$B";
my @L3 = grep /^Maxi/, readdir REP3;
closedir REP3;
my $tous;
foreach $tous (@L3){
  #rmdir ("$dir2$ff$WC$B$tous");  ## On supprime si c'est vide
  if (-e "$dir2$ff$WC$B$tous" == 1) {system("mv $dir2$ff$WC$B$tous $dir$Intermed$ff");}
}

# ecriture des bilans
my $fileBilan="$dir\/BilanSEMMsup1.recup";
$valpg=system(dirname($0)."/LIB_PERL_R/RecupToutesInfo_PAS.pl --Rep $dir\/MotifsDInteret/surRepresente/ --SEMM $seuilIC --out $fileBilan --format $format --LimInfPP $LimInfPP");
if($valpg != 0 ){print "*** Error : $valpg $!***\n";print LOG "*** Error : $valpg $!***\n";}
else{print LOG "-- RecupInfo OK \n";}

#Ajout dans les bilans de la colonne annotation si l'utilisateur le souhaite
my $fichierBilan = "$dir\/BilanSEMMsup1.recup_ALL";
if($BD eq "y"){
	$valpg=system(dirname($0)."/LIB_PERL_R/BD.pl -fichier $fichierBilan");
	if($valpg != 0 ){print "*** Error : $valpg $!***\n";print LOG "*** Error : $valpg $!***\n";}
}

# si on veut garder les fichiers intermediaires
system("rm -R $Res$WC\/");
system("rm -R $dir/data");

# Bilan

if($! eq ""){ # pas de pb tout est ok
	# comptage nbr de PLM trouves
	$fileBilan=$fileBilan."_ALL";
  	print LOG "\n### RESULTS ###\n\n";
	my $nbPLM=`wc -l $fileBilan`;
	print LOG "PLM found : ".($nbPLM -1);	

	print LOG "\n  --> Find all the PLM with SMS > $seuilIC peak in \n$dir/BilanSEMMsup1.recup_ALL";
	print LOG "\n  --> All the gene lists in directory $dir"."MotifsDInteret/surRepresente/ListeMotifsInteret/*.ListeGenes";
	print LOG "\n      listing of gene containing motifs in their functional windows";
	print LOG "\n  --> And figures in directory $dir"."MotifsDInteret/surRepresente/*.bmp or *.pdf\n\n";

	print LOG "\n ##Analysis informations :\n";

	#Informations sur le traitement effectué
	print LOG "\n  --> Date : $Date";
	print LOG "\n  --> Sequences file used for analysis : $fSeq";
	print LOG "\n  --> Motifs file used for analysis : $fMotif\n";
	print LOG "\n  IMPORTANT : Graphics and gene lists are available in PLM_graphs and PLM_lists\n";

	#Résumé des paramètres utilisés (à garder à chaque nouveau lancement pour réaliser des comparaisons futures)
	print LOG "\n  --> Parameters values - scoreIC : $seuilIC, strand(s) : $brin, alpha : $alpha, PP lower limit : $LimInfPP, PP upper limit : $CHEMIN::LimSupPP, Min FF size : $CHEMIN::LimInfL, Max FF size : $CHEMIN::LimSupL, Minimum number of sequences : $CHEMIN::NbSeq  \n";

	#Ajout des informations sur le lissage (fenêtre glissante)	
	if($lissage eq ""){
		print LOG "\n  --> Size of the sliding window : default (increasing size)\n\n";
	}else{
		print LOG "\n  --> Size of the sliding window : $lissage\n\n";
	}
	
	print LOG "\n PLMs are sought between $LimInfPP and $CHEMIN::LimSupPP\n\n";
	
}
close(LOG);

#Renommage final des fichiers images et listes de gènes
$valpg=system(dirname($0)."/LIB_PERL_R/Renommage.pl -dir $dir -fichier $fichierBilan");
if($valpg != 0 ){print "*** Error : $valpg $!***\n";print LOG "*** Error : $valpg $!***\n";}

