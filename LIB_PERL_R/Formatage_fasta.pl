#!/usr/bin/env perl
use strict;
use Carp;
use Cwd;
use Getopt::Long;

#Script pour formater les fichiers fasta avec des mauvais noms de séquences à cause de bedtools
my $file;
my $output;

GetOptions ("file:s" => \$file,
            "output:s" => \$output); #$file fichier fasta à traiter 

if ($file eq "" || !(-e $file)){
    print "usage : Formatage_fasta.pl
	-file  <fichier à formater>
    -output <nom du fichier de sortie>\n";
	exit;
}

open FILE, $file;
open(OUTPUT,">$output")|| die ("$output $!\n");
while(<FILE>){
    chomp($_);
    if($_ =~ m/^>/){
        (my $geneID, my $extension) = split(/\:/, $_);
        print OUTPUT "$geneID\n";
    }else{
        print OUTPUT "$_\n";
    }
}
close FILE;
close OUTPUT;