#!/usr/bin/env perl

use Carp;
use strict;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre ou est le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";

## But du prog : etude de tous les fichiers contenant les comptages 
# (fichiers binvalues) et donne la position du pic

## declaration des variables -> les repertoires
my $WC = "/WordCounter/";
my $Comptages = "/WC2geneNumber/";
## Pgm R
my $progR = "/ConstructionGraphes_Double_Lissage.R";

## Donnees pas Defaut
my $ALPHA=$CHEMIN::alphaIC;
my $brin = $CHEMIN::strand;
my $START=$CHEMIN::DEB; # sequence debut par rapport au TSS(-1000)
my $STOP=$CHEMIN::FIN; # sequence fin p/r au TSS
my $LEARNING=$CHEMIN::APPRENT; # zone d'apprentissage
my $graduation=$CHEMIN::grad; # graduation pour graphe
my $LA1 = $CHEMIN::DEB;
my $LA2 = $CHEMIN::DEB + $CHEMIN::APPRENT;

my $fileWC;
my $d;
my $f;

my $PicKO1 = "/MaxiDansRef/";
my $PicKO2 = "/MaxiSousSeuil/";

my $TFA;
my $TEMPO;
my $Res;
my $SeuilIC = $CHEMIN::seuilIC;
my $lissage = $CHEMIN::lissage;
my $format = $CHEMIN::form;
my $arrondi = $CHEMIN::arrondi;
my $UTR = $CHEMIN::UTR;
my $doublelissage = $CHEMIN::doublelissage;
my $publi = $CHEMIN::publi;
##########################################################################################################

## On recup les donnees en ligne de commande
GetOptions ("brin=s" => \$brin,
	    "Res=s" => \$Res,
	    "alpha:s" => \$ALPHA,
	    "tfa:s" => \$TFA,
	    "data:s" => \$TEMPO,
	    "seuilIC:s" => \$SeuilIC,
	    "lissage:s"=> \$lissage,
	    "format:s"=> \$format,
	    "arrondi:s"=> \$arrondi,
	    "UTR:s"=>\$UTR,
	    "smooth:s"=>\$doublelissage,
	    "publi:s"=>\$publi,
      "LA1:s"=>\$LA1,
      "LA2:s"=>\$LA2);

############################################################################################
############################################################################################

$START = $LA1;
$LEARNING = $LA2 - $LA1;

## Preparation des reprtoires de sortie

if(!(-e "$Res$WC")){
	mkdir ("$Res$WC") || die "Cannot mkdir $Res$WC : $!";
}
my $Pen = "Bilan/";
# Repertoires qui vont contenir les resultats
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mkdir ("$Res$WC$Pen") || die "Creation repertoire $Res$WC$Pen : $!";
mkdir ("$Res$WC$Pen$Comptages") ||  die "Creation repertoire $Res$WC$Pen$Comptages : $!";
mkdir ("$Res$WC$Pen$PicKO1")    ||  die "Creation repertoire $Res$WC$Pen$PicKO1 : $!";
mkdir ("$Res$WC$Pen$PicKO2")    ||  die "Creation repertoire $Res$WC$Pen$PicKO2 : $!";

############################################################################################
## On fait les pretraitement dans le fichier qui contient la liste des fichiers binvalues ##
############################################################################################
my $file;
my $BORNE2;
opendir REP,"$TEMPO";
my @fichiers = grep /binvalues$/, readdir REP;
my $liste = grep /^ListeFichiersBinvalues/, readdir REP;


closedir REP;

foreach $file (@fichiers){
  ## Lancement du prog de recherche des pics
  # on prepare le fichier qui contient les commandes
  open (TEMPO,">$$\_commandes") || die "Problem $$\_commandes:$!";
  print TEMPO "source (\"$pgmDir$progR\")\nlancement.prog(\"$TEMPO$liste\",\"$TEMPO$file\",$ALPHA,\"$Res$WC\",$SeuilIC,$START,$STOP,$LEARNING,$graduation,\"$lissage\",formatImage=\"$format\",\"$arrondi\",\"$UTR\",\"$doublelissage\",\"$publi\")\n";
  close(TEMPO);
 
  # l'option vanilla permet de ne pas sauver ni restaurer
  system ("R --vanilla --silent < $$\_commandes > $$\_bilan");
}

## Recup tous les graphes generes
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
opendir REP,"$Res$WC";

my @fichiers;

if($format eq "bmp"){
	 @fichiers = grep /$CHEMIN::plotbmp$/, readdir REP;
}elsif($format eq "pdf"){
	 @fichiers = grep /$CHEMIN::plotpdf$/, readdir REP;
}
closedir REP;

#############################################
## realisation des comptages pour le bilan ##
#############################################
my $fich;

foreach $fich(@fichiers){
  if ($fich =~ m/apprentissage/) {
    system("mv $Res$WC$fich $Res$WC$Pen$PicKO1$fich");
  }
  elsif ($fich =~ m/trand_00/) {
    system("mv $Res$WC$fich $Res$WC$Pen$PicKO2$fich");
  } 
}


## ON TRAITE TOUS LES MOTIFS POUR LESQUELS ON A TROUVE UN PIC
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

opendir REP,"$Res$WC";
@fichiers = grep /POSITIONS.txt$/, readdir REP;
closedir REP;

my $BornesDeD;
my $BornesDeF;
my $motif;
foreach $fich(@fichiers){
  ## On recup les positions de debut et fin de pic dans le fichier
  open(IN,"$Res$WC$fich");
  while (<IN>){
    if ($_ =~ /^BornesPic\s+(-?\d+)\s+(-?\d+)\s+/){
      $BornesDeD = $1;
      $BornesDeF = $2;}
    elsif ($_ =~ /^Motif\s+(\S+)\s+/) {$motif = $1;}
  }
  close(IN);

  system("$pgmDir/GenereListeAT.pl --mot $motif --TFA $TFA --D $BornesDeD --F $BornesDeF  --adresse $Res$WC$Pen$Comptages  --brin $brin --LA1 $LA1");

  unlink ("$Res$WC$fich");
}
#############################################
#Supprime le fichier de commandes destine a R
system("rm $$\_bilan $$\_commandes"); 
