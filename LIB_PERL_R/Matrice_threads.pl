#!/usr/bin/env perl

use strict;
use warnings;
use Carp;
use Getopt::Long;
use File::Basename;

# Connaitre d'ou est lancé le pgm
my $pgmDir=dirname($0);

#Programme pour réaliser la matrice finale gène/PLM

##################################################################################################################

my %matrice = ();
my $File;
my $motif;
my @tab;
my $dir;
my %motifs ;

GetOptions ("dir:s" => \$dir); #$dir étant le répertoire des résultats

if ($dir eq ""){
    print "usage : Matrice_threads.pl
	-dir  <répertoire des résultats>\n";
	exit;
}

	
my $listdir = $dir."/PLM_lists/";
my $graphdir = $dir."/PLM_graphs/";


#Récupération des infos gènes dans les fichiers de listes
opendir(my $dl, $listdir) || die ("$listdir $!\n");
while (readdir $dl) {
	if($_ =~ m/listgenes.txt$/){
		@tab = split(/_/, $_);
		$motif = $tab[0];
		my $file = $listdir."/".$_;
		#ouverture et traitement d'un fichier motif
		open GENE, $file or die "$!: $_";
			
		#parcours du fichier et récupération des gènes	
		while(<GENE>){
			chomp($_);
			if(!($_ =~ m/^#/)){

				$matrice{$_}{$motif}=1;
			}
		}
		close GENE;
	}
}

closedir $dl;	

#Récupère les informations sur les PP pour chaque PLM dans le dossier PLM_graphs
opendir(my $dg, $graphdir) || die "Can't open $graphdir: $!";
while (readdir $dg) {
	if($_ =~ m/pdf$/){
		@tab = split(/_/, $_);
		my $motif = $tab[0];
		(my $PP, my $pdf) = split(/\./, $tab[1]);
		$motifs{$motif}=$PP;
	}
	#Si il y a des fichiers bmp et non pdf
	if($_ =~ m/bmp$/){
		@tab = split(/_/, $_);
		my $motif = $tab[0];
		(my $PP, my $pdf) = split(/\./, $tab[1]);
		$motifs{$motif}=$PP;
	}
}

closedir $dg;	


#Ecriture de la matrice de présence dans un fichier
my $file = $dir."/PLM_genes.txt";

open(MATRICE,">$file")|| die ("$file $!\n");

#Affichage des motifs (titre des colonnes)
foreach my $titre(sort ({$motifs{$a} <=> $motifs{$b} } keys %motifs)){
	print MATRICE "\t$titre\_$motifs{$titre}";
}

print MATRICE " \n";

#Ecriture des genes avec présence ou absence du motif
foreach my $gene (keys %matrice){

	print MATRICE "$gene\t";

	foreach my $mot (sort ({$motifs{$a} <=> $motifs{$b} } keys %motifs)){
		if (exists($matrice{$gene}{$mot})){
			print MATRICE "1\t";
		}
		else{
			print MATRICE "0\t";
		}
	}
	print MATRICE "\n";	
}

close MATRICE;	

## Pgm R
my $progR = "/SortMatrix.R";

open (TEMPO,">$$\_tri") || die "Problem $$\_tri:$!";
print TEMPO "source (\"$pgmDir$progR\")\nSortMatrix(\"$file\")\n";
close(TEMPO);
 
# l'option vanilla permet de ne pas sauver ni restaurer
system ("R --vanilla --silent < $$\_tri > $$\_bilan");

#Supprime le fichier de commandes destine a R
system("rm $$\_bilan $$\_tri"); 
