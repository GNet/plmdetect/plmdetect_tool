#!/usr/bin/env perl

use strict;
use Carp;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre ou est le pgm
my $pgmDir=dirname($0);

require "$pgmDir/CHEMIN.pm";
require "$pgmDir/SEQUENCES.pm";

#################################################################################################################
#Programme qui permet de lister les ID des genes possedants dans leur promoteur                                 #
#le motif donne entre les bornes donnees en argument                                                            #
#################################################################################################################

my $adresse; ## ou mettre les resultats?
my $TFA;  ## fichier .tfa a etudier
my $D;
my $F;
my $mot;
my $brin;
my $LA1 = $CHEMIN::DEB;

## Recup des donnees de la ligne de commande
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GetOptions ("brin:s" => \$brin,
	    "D=i" => \$D,
	    "F=i" => \$F,
	    "motif=s" => \$mot,
	    "adresse=s" => \$adresse,
	    "TFA=s" => \$TFA,
      "LA1=s" => \$LA1);


if($brin eq "" || $D eq "" || $F eq "" || $mot eq "" || $adresse eq "" || $TFA eq ""){
    print "usage :
-brin <1 ou 2 brins>
-D <fin de pic>
-F <debut de pic>
-motif <motif (code IUB ok)>
-adresse <ou ranger>
-TFA <fichier tfa>\n";
    exit;
}
if($brin == 2){$brin = $CHEMIN::strand;}
elsif($brin != 1){print "-strand value is only 1 or 2 \n";exit;}


## On cree le repertoire de sortie...
if (-e "$adresse" != 1){mkdir ("$adresse") || die "mkdir $adresse : $!";}
#######################################################################################
#######################################################################################
## Recup toutes les seq du fichier .tfa
open(IN,"$TFA")|| die "can not open $TFA:$!";
my $ID;
my %seq_tous; 
while (<IN>){
  if ($_ =~ /^>(\S+)/){$ID = $1;}
  if ($_ =~ /^(\w+)\s/){$seq_tous{$ID} = "$seq_tous{$ID}$1";}
}
close(IN);
#######################################################################################
#######################################################################################
## On recup le pic + taille motif -1 positions afin que les motifs a cheval sur la fenetre soient comptes
my %pic_tous;
my %pic_tous2;
my $ListeSeqP = "$adresse\/ListeSeqTsPicBrinP.txt";
my $ListeSeqM = "$adresse\/ListeSeqTsPicBrinM.txt";


## CAS DU BRIN+
my $FinP = $F+length($mot)-1;
system ("$pgmDir/RecupBoutSeq.pl --de $D --a $FinP --output $ListeSeqP --ID yes --tfa $TFA --LA1 $LA1");
open(IN,"$ListeSeqP") || die "$!"; 
while (<IN>){
  if ($_ =~ /(\S+) (\S+)/){$pic_tous{$1} = $2;}
}

my $Strand = "+Strand";

## CAS DU BRIN-
if($brin eq $CHEMIN::strand){
  $Strand = "+and-Strand";
  my $APartirDe = $D-length($mot)+1;
  if($APartirDe < $LA1){$APartirDe  = $LA1;}
  system ("$pgmDir/RecupBoutSeq.pl --de $APartirDe --a $F --output $ListeSeqM --ID yes --tfa $TFA --LA1 $LA1");
  open(IN,"$ListeSeqM") || die "$!";
  while (<IN>){
	  if ($_ =~ /(\S+) (\S+)/){$pic_tous2{$1} = $2;}
  }
}


#######################################################################################
#######################################################################################
## Recup les seq de pic avec le motif
my %pic_interet;
my @ListeMot = &SEQUENCES::IUBtoListe($mot);

my $M;
foreach (keys(%pic_tous)){
  my $recup  = $pic_tous{$_};
  my $recup2 = $pic_tous2{$_};
  my $i = 0;
  while ($i <= $#ListeMot && $pic_interet{$_} eq ""){
    $M = $ListeMot[$i];

    ## recherche sur le brin +
    my $I = index($recup,$M);
    if ($I != -1){
      $pic_interet{$_} = $pic_tous{$_};
    }
    ## recherche sur le brin - si c'est demande
    elsif ($brin eq $CHEMIN::strand ){
      my $I2 =  index($recup2,&SEQUENCES::ComplemInv($M));
      if ($I2 != -1) {
	      $pic_interet{$_} = $pic_tous{$_};
      }
    }
    $i+=1;
  }
}

#######################################################################################
#######################################################################################
## -> pour creer le fichier
my $Debut = rindex($TFA,"/")+1;
my $P1 = substr($TFA,$Debut,length($TFA));

## Ecriture dans le fichier de sortie ListeGenes
my $OUT1 = "$adresse$P1\_$mot\_$D\_$F\_$Strand.ListeGenes";
open(OUT1,">$OUT1") || die " creation $OUT1:$!";
print OUT1 "#Occurences of $mot between $D and $F (positions relative to the transcription start site)\n";
my @Ts = keys(%pic_interet);
my $somme2 = $#Ts+1;
print OUT1 "#$somme2 sequences with $mot in this interval\n";

my @COMPTE = (keys(%seq_tous));
my $arrondi = sprintf("%.2f",100*$somme2/($#COMPTE+1));
print OUT1 "#$arrondi% of the sequences in promoter set with $mot (set with ",$#COMPTE+1," sequences)\n";
foreach (keys(%pic_interet)){print OUT1 "$_\n";}
close(OUT1);

system ("rm $adresse\/ListeSeqTsPic*");

