rm(list =ls())
## fonction pour un ens de motifs, qui sont analyses independemment
## les uns des autres

#N'affiche pas les warnings dans le terminal dus à la fonction rug() utilisée pour les graphiques
options(warn=-1)

########################################################################
#Pour effectuer des tests
#apprenti=700
#grad=100
#deb  = -1000
#fin  = 500
#alpha=0.01
#LIMITEliss=100
#fileIN="/home/jrozier/Bureau/Test R/data/TEMPO/7375/ListeFichiersBinvaluescluster_1_promoteur.fasta"
#ou ="/home/jrozier/Bureau/Test R/"
#fileIN2 = "/home/jrozier/Bureau/Test R/cluster_1_promoteur.fasta_+and-Strand_CAACA_relTSS_pas1.binvalues"
#fileIN2 = "/home/jrozier/Bureau/Test R/cluster_1_promoteur.fasta_+and-Strand_TATAWA_relTSS_pas1.binvalues"
#fileIN2 = "/home/jrozier/Bureau/Test R/cluster_1_promoteur.fasta_+and-Strand_KCAYW_relTSS_pas1.binvalues"
#fileIN2 = "/home/jrozier/Bureau/Test R/cluster_1_promoteur.fasta_+and-Strand_CAACA_relTSS_pas1.binvalues"
#SeuilIC = 1
#arrondi = "F"
#UTR = 5
#formatImage = "pdf"
#liss = ""
#LIMITEliss = 100
#doublelissage="n"
#publi = "F"
########################################################################

###########################################################################################################################################

#fileIN correspond au fichier ListeFichiersBinvalues
#fileIN2 correspond au fichier binvalue traité
#alpha : seuil de l'intervalle de confiance
#ou : le répertoire de sortie pour les images
#SeuilIC : score minimal pour considérer que le pic est bien intéressant (par défaut 1)
#deb : la position de début de la séquence par rapport au TSS
#fin : de même pour la fin
#apprenti : taille de la zone d'apprentissage
#grad : la graduation des graphiques 
#liss : valeur de la taille de la fenetre glissante choisie par l'utilisateur (par défaut la fenêtre est croissante et liss vaut "")
#formatImage : actuellement "bmp" ou "pdf"
#arrondi : booléen indiquant si l'utilisateur souhaite arrondir à 5 paires de bases près les bornes de la FF
#LIMITEliss : taille maximale de la fenêtre glissante (fixée à 100)
#UTR : prend la valeur 3 ou 5 et indique quelle région est analysée (pour écrire TSS ou TTS)
#publi : booléen indiquant si l'utilisateur souhaite faire un rendu en noir et blanc
###########################################################################################################################################

lancement.prog<-function(fileIN,fileIN2,alpha,ou,SeuilIC,deb,fin,apprenti,grad,liss,formatImage,arrondi,UTR,doublelissage,publi,LIMITEliss=100){
  ## parametres 
  extension=""
  alpha <- 1-alpha
  graduation <-seq(deb,fin,grad)
  ## parametres graphiques globaux (TSS placé à 0)
  axis.label=graduation
  
  if(UTR == 5){
    axis.label[which(axis.label==0)] <- "TSS"
  }
  else{
    axis.label[which(axis.label==0)] <- "TTS"
  }
  Ylegende="Number of promoters"
  Xlegende=""
  
  ### RECHERCHE DE PICS
  ## Lecture du fichier
  
  #F contient les éléments contenu dans le fichier binvalue
  F <- scan(file=fileIN2,quiet = TRUE,what = "character",blank.lines.skip = FALSE)
  #Fnom contient le nom du fichier binvalue utilisé ensuite dans la fonction ModifNomFichier
  Fnom <- fileIN2
  
  #borne sera la position séparant la zone d'apprentissage et la zone de prédiction
  borne <- apprenti 
  
  
  ## Recup le motif a etudier
  motif = strsplit(strsplit(fileIN2,split="Strand_")[[1]][2],split="_relTSS")[[1]][1]
  cat("\n",motif," etudie : ")
  ## parametres graphiques lie au motif
  Tlegende=paste0("Distribution of ",motif)
  
  #L contient la liste des gènes possédant le motif sur l'ensemble des positions
  L <- strsplit(F,split="_") 
  
  #y est le vecteur contenant le nombre de gènes ayant le motif recherché à la position donnée
  y<-unlist(lapply(strsplit(F,"_"),length)) 
  
  #On retire la dernière valeur du vecteur car celui ci fait une taille de 1501 au lieu de 1500 (point pouvant être corrigé en amont)
  y <- y[-length(y)] 
  #x correspond à l'ensemble de la région étudiée soit de -1000 à 500
  x <- deb:(fin-1) 
  
  #Si l'ensemble des séquences comporte au moins une fois le motif 
  if (mean(y) != 0) 
  {
    yS <- y #yS comporte l'ensemble des nombres de gènes possédant le motif à la position souhaitée
    NEW<-y #NEW possède l'ensemble des nombres de gènes possédant le motif à la position souhaitée
    
    #POUR UNE TAILLE DE FENETRE GLISSANTE CROISSANTE (ADAPTABLE)
    if(liss == ""){
      lissage <- 0
      
      ## modif de NEW[1:borne] en NEW[1:(borne+1)] car sinon apprentissage## sur -1000 ? -301 et non -300 
      #Expression pour définir les conditions pour que le lissage continue
      #Tant que le lissage n'a pas atteint la limite et qu'il y a des valeurs nulles dans la zone d'apprentissage
      
      while  (lissage < LIMITEliss  && (sum(NEW[1:(borne+1)] == 0)) >= 1 ){
        #augmentation du lissage (taille de la fenêtre glissante) et diminution de 1 de borne
        lissage <- lissage + 1 
        borne <- borne -1 
        
        #g.tmp.inf prend toutes les valeurs de la borne inférieure de la fenêtre glissante, g.tmp.sup celles de la borne supérieure
        g.tmp.inf<-seq(1,(length(y)-lissage),1) 
        g.tmp.sup<-g.tmp.inf+lissage 
        
        #A chaque élément du vecteur 1 à la taille de g.tmp.inf, il applique les valeurs du vecteur L des indices g.tmp.inf à l'indice x jusqu'à
        #g.tmp.sup à l'indice x -> Contient le nom des gènes contenus entre g.tmp.inf et g.tmp.sup (passage de la fenêtre glissante)
        tempo.tmp<-sapply(1:length(g.tmp.inf),function(x) L[g.tmp.inf[x]:g.tmp.sup[x]])
        
        #On retire les potentielles redondances se trouvant dans le vecteur tempo.tmp
        NEW<-apply(tempo.tmp,2,function(x) length(unique(unlist(x)))) 
        
        #Retire la dernière valeur du vecteur x
        x <- x[-length(x)] 
      }
    }
    
    #POUR UNE TAILLE DE FENETRE GLISSANTE FIXE  
    else{
      
      #Suit les mêmes étapes que la fenêtre adaptable mais ne teste pas de condition et ne passe qu'une seule fois sur les données
      lissage = as.numeric(liss)
      g.tmp.inf<-seq(1,(length(y)-lissage),1)
      g.tmp.sup<-g.tmp.inf+lissage 
      tempo.tmp<-sapply(1:length(g.tmp.inf),function(x) L[g.tmp.inf[x]:g.tmp.sup[x]])
      NEW<-apply(tempo.tmp,2,function(x) length(unique(unlist(x)))) 
      x <- x[1:(length(x)-lissage)]
    }
    
    #y prend alors les valeurs du vecteur après le passage de la fenêtre glissante 
    y <- NEW 
    cat ("fenetre glissante de taille ",lissage)
    
    ## REGION D'APPRENTISSAGE
    
    #le new correspond à la zone d'apprentissage (-1000 à -300)
    new <- data.frame(x =deb:(deb+borne-1)) 
    
    #On réalise une régression linéaire sur cette zone, on obtient ainsi l'intervalle de confiance. (indispensable pour le calcul des scores)
    pred.w.clim <- predict(lm(y[1:borne] ~ x[1:borne]), new,interval="prediction",level=alpha)
    
    #debY2 : bornes supérieures de l'intervalle de confiance pour chaque position dans la distribution (upperbound)
    debY2 <- as.vector(pred.w.clim[,3]) 
    #debY : Valeurs de la régression (fitted values)
    debY  <- as.vector(pred.w.clim[,1])  
    
    ## REGION DE PREDICTION
    
    #First est la position de début de la région de prédiction et Last la fin
    First <- deb + length(new$x) 
    Last  <- First+(length(new$x)-1)
    
    #Y2 est le vecteur qui comportera les valeurs upperbound de la région de prédiction
    Y2 <- c() 
    #Y2 est le vecteur qui comportera les fitted values de la région de prédiction
    Y <- c()
    
    #X prend les valeurs en abscisse de la région de prédiction (de 701 à 1500 environ)
    X <- x[(borne+1):length(x)] 
    
    #On réalise la prédiction jusqu'à la fin de la région 
    while(First < fin){
      new2 <- data.frame(x=First:Last)
      pred.w.plim <- predict(lm(y[1:borne] ~ x[1:borne]), new2, interval="prediction",level = alpha)
      Y2  <-  c(Y2,as.vector( pred.w.plim[,3]))
      Y   <-  c(Y, as.vector( pred.w.plim[,1]))
      First <- First + length(new$x)
      Last  <- First + length(new$x) - 1
    }
    
    #MMM sert lors de la construction pour délimiter les bornes en ordonnées
    MMM <- max(Y2) 
    
    #Concatenation de la zone d'apprentissage et de prediction
    Y3 <- c(debY2,Y2) # upper bound
    Y4 <- c(debY,Y) # fitted values
    
    
    
    ##Le programme ne garde la position que de la plus grande valeur si deux pics sont de même hauteur
    
    #depasse correspond à la différence entre les valeurs de y et les upperbound de l'intervalle de confiance
    depasse <- y-Y3[1:length(y)]
    
    #PMax contient la valeur du pic maximal (relativement à l'intervalle de confiance)
    PMax = which.max(depasse) 
    #Coordonne sur le promoteur du pic (l'abscisse de ce point)
    MAXI <- x[PMax] 

    ##SI LE SEUIL N'EST PAS DEPASSE
    if (depasse[PMax]<0)
    {
      MMM2 <- max(MMM,y) 
      NOM <- ModifNomFichier(ou,motif,Fnom,lissage,Y3[PMax],Y4[PMax],depasse[PMax],"",formatImage)
      
      #Si le format d'image souhaité est .bmp
      if(formatImage == "bmp"){
        bitmap(NOM,height=8,width=8)
      } 
      #Si le format d'image souhaité est .pdf
      else{
        pdf(NOM, height = 8, width = 8)
      }  
      
      if(publi == "T"){
        matplot(new$x,col="black",cbind(pred.w.clim)[,c(1,3)], type="l",xlim = c(x[1],x[length(x)]),cex.lab=1.5,cex.main=1.5,ylab=Ylegende,xlab=Xlegende,main = Tlegende,ylim = c(0,MMM2),xaxt="n", cex.lab = 0.9)
      }
      else{
        matplot(new$x,col="red",cbind(pred.w.clim)[,c(1,3)], type="l",xlim = c(x[1],x[length(x)]),cex.lab=1.5,cex.main=1.5,ylab=Ylegende,xlab=Xlegende,main = Tlegende,ylim = c(0,MMM2),xaxt="n", cex.lab = 0.9)
      }
      lines(x,y)
      
      #rug : fonction qui ajoute une "couverture" (une graduation et spécifie la taille sur le graphique)
      rug(graduation,ticksize=0.01)
      #ajout de l'axe des abscisses avec la graduation du départ et l'annotation du TSS en 0
      axis(1,graduation,axis.label, cex.axis = 0.7) 
      #ajout un axe à droite
      axis(4) 
      
      #Mise en place de la légende 
      legend("topright", box.lty = 0, legend = paste("Sliding Window :",lissage), cex = 0.8)
      
      #Trace une droite verticale en pointillés en 0 (TSS)
      abline(v=0, lty=2)
      
      #Trace les droites
      Y <- Y[1:length(X)]
      Y2 <- Y2[1:length(X)]
      
      if(publi=="T"){
        lines(X,Y,col="black")
        lines(X,Y2,col="black",lty = 2)
      }
      else{
      lines(X,Y,col="blue")
      lines(X,Y2,col="green",lty = 2)
      }
      dev.off(which=dev.cur())
    }
    
    ###SI LE SEUIL EST DEPASSE
    else{
      ## Recherche de la premiere position SOUS le seuil a gauche du sommet --> debut de pic sera la position juste en aval
      gauche <- PMax
      while (depasse[gauche]>0 && gauche>1){gauche=gauche-1} 
      gauche = gauche+1
      
      #Yg : contient toutes les valeurs en ordonnées du début du pic jusqu'au max
      Yg <- y[gauche:PMax]
      #Xg : contient toutes les valeurs en abscisse du début du pic jusqu'au max
      Xg <- gauche:PMax 
      
      ## Même raisonnement pour la droite
      droite <- PMax
      while (depasse[droite]>0 && droite < length(x)) {droite=droite+1}
      droite = droite-1
      Yd <- y[PMax:droite]
      Xd <-  PMax:droite
      
      #VerifDep : Valeur du score au niveau du pic
      VerifDep <- round((depasse[PMax])/(Y3[PMax]-Y4[PMax]),2)+1 
      
      
#############################################DOUBLE LISSAGE SI DEMANDE######################################################################

##Basé sur le code de Virginie (ConstructionGraphes.R)
      
      if(doublelissage == "y"){
        NEW <- y
        dep <- 0
        
        if ( (1+Xd[length(Xd)]-Xg[1]) < length(which(depasse >0)) ){
          tempo <- depasse
          tempo [(Xd[length(Xd)]:Xg[1])] = 0
          ListeSansPicSup0 <- which(tempo > 0)
          Val <- depasse[ListeSansPicSup0]
          Qui <- which.max(depasse[ListeSansPicSup0])
          ValeurDepasse <- depasse[ListeSansPicSup0[Qui]]
          dep <- round((ValeurDepasse)/(Y3[ListeSansPicSup0[Qui]]-Y4[ListeSansPicSup0[Qui]]),2)+1
        }
        
        VerifDep <- round((depasse[PMax])/(Y3[PMax]-Y4[PMax]),2)+1 	## SEMM des sommets secondaires
        DernierPic <- Xd[length(Xd)]+deb+1				## derniere posi du "pic"
        
        ## Conditions d'arret de la boucle
        # Derniere posi du pic != fin de l'intervalle d'etude
        # ET taille de la fenetre glissante inferieure ? 100
        # ET le SEMM hors pic est > ? 1 et > ? 50% du SEMM du pic
        while  (DernierPic < (fin-lissage) && lissage < LIMITEliss && (dep*2 > VerifDep) && dep > 1){
          
          lissage <- lissage + 1
          borne <- borne -1
          NEW <- c()
          
          for(g in 1:(length(yS)-lissage)){
            tempo <- c()
            for (k in g:(g+lissage)){tempo <- c(tempo,L[[k]])}
            NEW[g] <- length(unique(tempo))
          }
          x <- x[-length(x)] 
          
          # Region d'apprentissage
          new <- data.frame(x = c(deb:(deb+borne-1)))
          pred.w.clim <- predict(lm(NEW[1:borne] ~ x[1:borne]), new, interval="prediction",level = alpha)
          debY2 <- as.vector(pred.w.clim[,3])
          debY  <- as.vector(pred.w.clim[,1])
          
          # Region de prediction
          First <- deb   + length(new$x)
          Last  <- First + length(new$x) - 1
          Y2 <- c() 
          Y <- c()
          
          while(First < fin){
            new2 <- data.frame(x = c(First:Last))
            pred.w.plim <- predict(lm(NEW[1:borne] ~ x[1:borne]), new2, interval="prediction",level = alpha)
            Y2  <-  c(Y2,as.vector( pred.w.plim[,3]))
            Y   <-  c(Y, as.vector( pred.w.plim[,1]))
            First <- First + length(new$x)
            Last  <- First + length(new$x) - 1
          }
          
          X <- x[(borne+1):length(x)]
          MMM <- max(Y2[1],Y2[2])
          Y3 <- c(debY2,Y2)
          Y4 <- c(debY,Y)
          depasse <- c()          
          for (t in 1:length(NEW)){depasse[t]<-NEW[t]-(Y3[t])}
          PMax = which.max(depasse) 
          MAXI <- x[PMax]
          
          #############################
          
          gauche <- PMax
          while (depasse[gauche]>0 && gauche > 1){gauche=gauche-1}
          gauche = gauche+1 
          Yg <- NEW[gauche:PMax]
          Xg <- c((PMax-length(Yg)+1):PMax)
          
          droite <- PMax
          while (depasse[droite]>0 && droite < length(x)){droite=droite+1}
          droite = droite-1
          Yd <- NEW[PMax:droite]
          Xd <- c(PMax:(PMax+length(Yd)-1))
          
          ## Un autre meilleur?
          dep <- 0
          if ( (1+Xd[length(Xd)]-Xg[1]) < length(which(depasse >0)) ){
            tempo <- depasse
            tempo [(Xd[length(Xd)]:Xg[1])] = 0
            ListeSansPicSup0 <- which(tempo > 0)
            Val <- depasse[ListeSansPicSup0]
            Qui <- which.max(depasse[ListeSansPicSup0])
            ValeurDepasse <- depasse[ListeSansPicSup0[Qui]]
            dep <- round((ValeurDepasse)/(Y3[ListeSansPicSup0[Qui]]-Y4[ListeSansPicSup0[Qui]]),2)+1
          }
          VerifDep <- round((depasse[PMax])/(Y3[PMax]-Y4[PMax]),2)+1
          DernierPic <- Xd[length(Xd)]+deb+1
        }
        y <- NEW
      }
 
########################################################################################################################################################################
      
      
      ##SEUIL DEPASSE MAIS LE PIC EST DANS LA ZONE APPRENTISSAGE (dans ce cas le graphique est gardé mais placé dans le dossier pour les éliminés intermédiaires) 
      if(PMax < borne){
        MMM2 <- max(MMM,y)
        extension <- "apprentissage"
        NOM <- ModifNomFichier(ou,motif,Fnom,lissage,Y3[PMax],Y4[PMax],depasse[PMax],extension,formatImage)
        
        #Si le format du fichier image est .bmp
        if(formatImage == "bmp"){
          bitmap(NOM,height=8,width=8)
        } 
        #Si le format du fichier image est .pdf
        else{
          pdf(NOM, height = 8, width = 8)
        } 
        
        #Production du graphique
        if(publi=="T"){
          matplot(new$x,col = "black",cbind(pred.w.clim)[,c(1,3)], type="l",xlim = c(x[1],x[length(x)]),cex.lab=1.5,cex.main=1.5,ylab=Ylegende,xlab=Xlegende,main = Tlegende,ylim = c(0,MMM2),xaxt="n", cex.lab = 0.9)
        }
        else{
          matplot(new$x,col = "red",cbind(pred.w.clim)[,c(1,3)], type="l",xlim = c(x[1],x[length(x)]),cex.lab=1.5,cex.main=1.5,ylab=Ylegende,xlab=Xlegende,main = Tlegende,ylim = c(0,MMM2),xaxt="n", cex.lab = 0.9)
        }
        score = paste("Score : ", VerifDep)
        
        #Mise en place de la légende 
        #Si l'utilisateur souhaite arrondir la fenetre fonctionnelle à 5 bases près
        if(arrondi == "T"){
          ffinter = paste(round((gauche+deb)/5)*5,ceiling((droite+deb)/5)*5, sep = ",")
          PP = paste(PMax+deb,"[")
          ffinter = paste(PP,ffinter)
          ffinter = paste(ffinter,"]")
          }
        
        #Si il ne le souhaite pas
        else{
          ffinter = paste(gauche+deb,droite+deb, sep = ",")
          PP = paste(PMax+deb,"[")
          ffinter = paste(PP,ffinter)
          ffinter = paste(ffinter,"]")
          }
        
        ff = paste("Functionnal window : ",ffinter)
        legend("topleft", box.lty = 0, legend = c(score,ff), cex = 0.8)
        legend("topright", box.lty = 0, legend = paste("Sliding Window :",lissage), cex = 0.8)
        
        lines(x,y)
        axis(1,graduation,axis.label, cex.axis = 0.7)
        axis(4)
        rug(graduation,ticksize=0.01)
        abline(v=0, lty=2)
        Y <-   Y[1:length(X)]
        Y2 <- Y2[1:length(X)]
        
        if (publi=="T"){
          lines(X,Y,col="black")
          lines(X,Y2,col="black",lty = 2)
        }
        else{
          lines(X,Y,col="blue")
          lines(X,Y2,col="green",lty = 2)
        }
#####################################################################################################################################        
        ##Si le double lissage est demandé et qu'il y a des pics secondaires, on marque le plus haut (celui qui dépasse le plus!) avec une étoile bleue
        if(doublelissage == "n"){
          dep <- 0
          if ( (1+Xd[length(Xd)]-Xg[1]) < length(which(depasse >0)) ){
            tempo <- depasse
            tempo [(Xd[length(Xd)]:Xg[1])] = 0
            ListeSansPicSup0 <- which(tempo > 0)
            Val <- depasse[ListeSansPicSup0]
            Qui <- which.max(depasse[ListeSansPicSup0])
            ValeurDepasse <- depasse[ListeSansPicSup0[Qui]]
            dep <- round((ValeurDepasse)/(Y3[ListeSansPicSup0[Qui]]-Y4[ListeSansPicSup0[Qui]]),2)+1
          }
          
          VerifDep <- round((depasse[PMax])/(Y3[PMax]-Y4[PMax]),2)+1
          
          if(dep*2 > VerifDep && dep > 1){
            if(publi == "T"){
              points(x[ListeSansPicSup0[Qui]],y[ListeSansPicSup0[Qui]],col="black",pch = '*',cex=2)
            }
            else{
              points(x[ListeSansPicSup0[Qui]],y[ListeSansPicSup0[Qui]],col="blue",pch = '*',cex=2)
            }
          }
        } 
#####################################################################################################################################        
        dev.off(which=dev.cur())
      }
       

      ##SEUIL DEPASSE ET PIC HORS DE LA ZONE D'APPRENTISSAGE 
      else{ 

        ##Pour fichier de sortie avec liste des positions du pic
        sommet <- c(x[PMax]) 
        picN <- c(x[Xg[1]],x[Xd[length(Xd)]])
        
        #Si on désire arrondir à 5 bases près la fenêtre fonctionnelle
        if(arrondi == "T"){
          picN[1] = round(picN[1]/5)*5
          picN[2] = ceiling(picN[2]/5)*5
        }
        
        if(extension == ""){extension <- paste0(MAXI,"peak")}
        
        NOM = ModifNomFichier(ou,motif,Fnom,lissage,Y3[PMax],Y4[PMax],depasse[PMax],extension,formatImage)
        
        #Choix du format
        if(formatImage == "bmp"){
          bitmap(NOM,height=8,width=8)
        } 
        else{
          pdf(NOM, height = 8, width = 8)
        }
        
        MMM2 <- max(MMM,y)
        
        if(publi == "T"){
          matplot(new$x,col = "black",cbind(pred.w.clim)[,c(1,3)], type="l",ylab=Ylegende,xlab=Xlegende,xlim = c(x[1],x[length(x)]),ylim = c(0,MMM2),main = Tlegende,cex.lab=1.5,cex.main=1.5,xaxt="n", cex.lab = 0.9)
        }
        else{
          matplot(new$x,col = "red",cbind(pred.w.clim)[,c(1,3)], type="l",ylab=Ylegende,xlab=Xlegende,xlim = c(x[1],x[length(x)]),ylim = c(0,MMM2),main = Tlegende,cex.lab=1.5,cex.main=1.5,xaxt="n", cex.lab = 0.9)
        }
        score = paste("Score : ", VerifDep)
        
        #Mise en place de la légende 
        ffinter = paste(picN[1],picN[2], sep = ",")
        PP = paste(PMax+deb,"[")
        ffinter = paste(PP,ffinter)
        ffinter = paste(ffinter,"]")
        ff = paste("Functionnal window : ",ffinter)
        legend("topleft", box.lty = 0, legend = c(score,ff), cex = 0.8)
        legend("topright", box.lty = 0, legend = paste("Sliding Window :",lissage), cex = 0.8)
        
        lines(x[1:Xg[1]],y[1:Xg[1]])
        axis(1,graduation,axis.label, cex.axis = 0.7)
        ##axis(4)
        rug(graduation,ticksize=0.01)
        abline(v=0, lty=2)
        lines(x[Xd[length(Xd)]:length(x)],y[Xd[length(Xd)]:length(x)])
        Y  <- Y[1:length(X)]
        Y2 <- Y2[1:length(X)]
        
        if(publi=="T"){
          lines(X,Y,col="black") ## moy
          lines(X,Y2,col="black",lty = 2) ## IC
        }
        else{
          lines(X,Y,col="blue") ## moy
          lines(X,Y2,col="green",lty = 2) ## IC
        }
        ##S'il y a un seul point de sommet on le met en evidence pas une etoile
        if ((!is.na(x[Xd[length(Xd)]]) && !is.na(x[Xg[1]])) && (x[Xg[1]] == x[Xd[length(Xd)]]))
          if(publi=="T"){
            points(x[Xg[1]],y[Xg[1]],col="red",pch = '*',cex=2)
          }
          else{
            points(x[Xg[1]],y[Xg[1]],col="red",pch = '*',cex=2)
          }
        
        ##Sinon on trace toutes les lignes du pic en rouge
        else{
          if(publi=="T"){
            lines(x[Xg],Yg,col="black")
            lines(x[Xd],Yd,col="black")
          }
          else{
            lines(x[Xg],Yg,col="red")
            lines(x[Xd],Yd,col="red")
          }
        }
        
#####################################################################################################################################         
        #Si le double lissage est demandé et qu'il y a des pics secondaires, on les marque avec une étoile bleue
        if(doublelissage == "n"){
          dep <- 0
          if ( (1+Xd[length(Xd)]-Xg[1]) < length(which(depasse >0)) ){
           tempo <- depasse
            tempo [(Xd[length(Xd)]:Xg[1])] = 0
            ListeSansPicSup0 <- which(tempo > 0)
            Val <- depasse[ListeSansPicSup0]
            Qui <- which.max(depasse[ListeSansPicSup0])
            ValeurDepasse <- depasse[ListeSansPicSup0[Qui]]
            dep <- round((ValeurDepasse)/(Y3[ListeSansPicSup0[Qui]]-Y4[ListeSansPicSup0[Qui]]),2)+1
          }
        
          VerifDep <- round((depasse[PMax])/(Y3[PMax]-Y4[PMax]),2)+1
        
          if(dep*2 > VerifDep && dep > 1){
            if(publi == "T"){
              points(x[ListeSansPicSup0[Qui]],y[ListeSansPicSup0[Qui]],col="black",pch = '*',cex=2)
            }
            else{
              points(x[ListeSansPicSup0[Qui]],y[ListeSansPicSup0[Qui]],col="blue",pch = '*',cex=2)
            }
          }
        } 
#####################################################################################################################################         
        dev.off(which=dev.cur())
        
        #VerifDep est le score
        VerifDep <- round((depasse[PMax])/(Y3[PMax]-Y4[PMax]),2)+1
        
        #Si le graphique présente toutes les caractéristiques pour être un PLM
        if (VerifDep >= SeuilIC && extension != "apprentissage" && extension != "apprentissageHS" && lissage < LIMITEliss){
          ## Quel fichier TFA??
          coupe <- strsplit(fileIN,split="ListeFichiersBinvalues")
          TFA <- paste (coupe[[1]][2],"tfa",sep = ".")	
          
          ## Quel motif??
          FILE <- ou
          FILE <- paste (FILE,TFA,sep = "/")
          FILE <- paste (FILE,motif,sep = "_")	
          FILE <- paste (FILE,"POSITIONS.txt",sep = "_")
          FILE <- paste(NOM,"POSITIONS.txt",sep="_")
          cat ("Motif ",motif,"\nSommet ",sommet[1]," ",sommet[1],"\nBornesPic ",picN[1]," ",picN[2],"\nSEMM ",VerifDep,"\n",file = FILE)
        }
      }
    }
  }
}

############################################################################
############################################################################

## arrange le nom du fichier pour qu'il soit de la forme 
## ValeurIC_motif_texteOptionnel.bmp
ModifNomFichier<-function(ou,motif,F,lissage,seuil,moy,depass,texte,formatImage){
  coupe <- strsplit(F,split="/")    
  lim <- length(coupe[[1]])-1
  if(ou != ""){Repertoire <- ou}
  else{
    Repertoire <- coupe[[1]][1]
    if (lim >= 1){
      for (p in 2:lim){Repertoire <- paste (Repertoire,coupe[[1]][p],sep = "/")}
    }
  }
  
  dep <- round((depass)/(seuil-moy),2)+1
  if((seuil-moy) == 0){
    if(depass!=0){
      dep = 99.99
    }else{
      dep = 0
    }
  }
  dep2 <- dep
  if (dep < 10){dep <-  paste(0,dep,sep = "")}
  f <- paste(strsplit(coupe[[1]][lim+1],split="Strand")[[1]][1],dep,sep = "Strand_")
  f <- paste(f,motif,sep = "_")
  Repertoire <- paste (Repertoire,f,sep = "/")
  Repertoire <- paste (Repertoire,lissage+1,sep="_Windows")
  if (texte != ""){Repertoire <- paste(Repertoire,texte,sep = "_")}
  if(formatImage == "bmp"){Repertoire <- paste(Repertoire,"bmp",sep = ".")}
  else{Repertoire <- paste(Repertoire,"pdf",sep = ".")}
    
  Repertoire
}

###########################################################################################
###########################################################################################

