#!/usr/bin/env perl
use DBI;
use strict;
use Carp;
use Getopt::Long;
use File::Basename;

# Connaitre d'ou est lance le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";

my %list;
my $id_feat;
my $id_feat2;
my $seq="";
my $flag;
my $ListeID;
my $Fsortie;
my $Genome="";

###########################################
###########################################
## On recup les donnees en ligne de commande
GetOptions ("ID=s" => \$ListeID,
	    "OUT=s" => \$Fsortie,
	    "tfa=s" => \$Genome);

if ($ListeID eq "" || $Fsortie eq ""){
	print "\nUSAGE :\n ./ListeAT2PromSeq.pl -ID <Fichier contenant une liste d'identifiants> -OUT <fichier qui sera genere> -tfa <fichier de promoteur fasta>\n";
	exit;
}

###########################################
###########################################
my $nbAV = 0;
my $nbAP = 0;
open (IN,"$ListeID") || die "ListeID : $!";
while ($_ = <IN>){
	if (($id_feat) = ($_ =~ /^(.+)\s/)){
      $id_feat = uc($id_feat);
      $list{$id_feat} = $id_feat;
      $nbAV++;
  	}else{
      print "$_ pb\n";
      exit;
  	}
}
close IN;
print "$nbAV genes to read\n";

open (SS,">$Fsortie") || die "$Fsortie:$!";
open (IN2,"$Genome") || die "$Genome:$!";
while (my $line= <IN2>){
	if ($line =~ m/^>(.+)\s/){
		$id_feat2=$1;
		# on ecrit la sequence lue
		print SS "$seq";
		# on garde que les sequences voulues
		if (defined $list{$id_feat2}){ 
			$seq=">$id_feat2\n";
	 		$nbAP++;
	 		$flag=1;
		}else{	
			# on reinitialise
			$id_feat2="";
			$seq="";
			$flag=0;
		}
  	}elsif($flag==1){
  		$seq.=$line;
	}
}
print SS "$seq";
close (IN2);
close (SS);
print "$nbAP sequences\n";
