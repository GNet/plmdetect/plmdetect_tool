#!/usr/bin/env perl
use strict;
use Carp;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre d'ou est lanc� le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";


###########################################################################################################################
###########################################################################################################################
#
# But du programme : generer les graphes illustrant la presence de motifs a une position donnee d'une sequence
# promotrice 
#
# USAGE : ./Representations_Graphiques.pl
###########################################################################################################################
###########################################################################################################################

## Declaration des variables
my $repertoire;                   ## nom du repertoire avec les listes de motifs
my @fichiers;                     ## tableau ds lequel on va recup les listes de motifs sur/sous representes
my $repTFA;                       ## nom du repertoire avec les fichiers .tfa
my $PAS = $CHEMIN::PasGenome;     ## taille des fenetre d'etude genome
my $LA1 = $CHEMIN::DEB;

my $f;
my @ListefichierWC;
my $fichierWC;
my @LrelTSS;
my $TSS;

## Repertoires utilises dans le script
my ($RepsurG,$RepsurbisG);
my $WC = "WordCounter/";

my $TFA;
my $REP;
my $brin = $CHEMIN::strand;
my $TEMPO;
##########################################################################################################################

## On recup les donnees en ligne de commande
GetOptions ("brin=s" => \$brin,
	    "repL=s" => \$repertoire,
	    "rangement=s" => \$REP,
	    "tfa=s" => \$TFA,
	    "data=s" => \$TEMPO,
      "LA1=s"=> \$LA1);


## Verif des noms de rep en argu

if(!(-e $REP)){
	mkdir ("$REP") || die "Cannot mkdir $REP : $!";
}
if(!(-e $repertoire)){
	mkdir ("$repertoire") || die "Cannot mkdir $repertoire : $!";
}

##########################################################################################################################

## Noms des repertoires utiles
$RepsurG = "/surRepresente/";


## on recupere tous les noms des fichiers contenant les listes de motifs sur representes
opendir REP,$repertoire;
@fichiers = grep /ok/, readdir REP;
closedir REP;

## Traitement des motifs
########################

## On prepare les repertoires 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mkdir ("$REP$RepsurG") || die "Cannot mkdir $RepsurG : $!";
mkdir ("$REP$RepsurG$WC") || die "Cannot mkdir $RepsurG$WC : $!";
$RepsurG="$REP$RepsurG";

## Fichier .tfa et fichier avec liste des motifs pour le wordcounter
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# les fichiers ayant les listes de motifs -> a concatener + supprimer les redondances et les CI
my %VerifMotif;
my $g = "/SurRepresentes.txt";
open (LISTEG,">$repertoire$g") || die "$repertoire$g:$!";
foreach $f (@fichiers){
 open (IN,"$repertoire$f") || die "$repertoire$f:$!";
  while (<IN>){
    chomp($_);
    if ($VerifMotif{$_} eq "")
      # si non redondance -> enregistre dans fichier
      {print LISTEG "$_\n";}
    $VerifMotif{$_} = $_;
  }
  close (IN);
}
close (LISTEG);

## On construit les graphes de comptage
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

my $valpg=system ("$pgmDir/WordCounter.pl --fileTFA $TFA --fileMot $repertoire$g  --brin $brin --ou $TEMPO --LA1 $LA1");
if($valpg != 0 ){print "*** Error : $valpg $!***\n";}
my $g2 = "/ListeGenomeSurrepresentes.txt";
system("mv $repertoire$g $repertoire$g2");


