#!/usr/bin/env perl

use strict;
use Carp;
use Getopt::Long;
use File::Basename;
use DBI;

# Connaitre d'ou est lancé le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";

##########################################################################################################

###Programme qui réalise les requêtes sur la BD pour les ajouter
my $fichier;

##Récupération des infos
GetOptions ("fichier=s" => \$fichier);

#la variable fichier comporte l'ensemble du chemin pour accéder au fichier bilan
if ($fichier eq ""){
  print "usage : BD.pl
	-fichier <Nom du fichier bilan à traiter>\n";
  exit;
}

#Création du nouveau fichier pour écrire les résultats
my $newfichier = $fichier."_1";

#ouverture et traitement d'un fichier motif
open PLM, $fichier || die ("$fichier $!\n");
open(PLM2,">$newfichier")|| die ("$newfichier $!\n");

##Initialisation des variables pour accéder à la BD
my $base = 'plm';
my $hote = 'dayhoff.ips2.u-psud.fr';
my $login = 'plmuser';
my $mdp	= 'plmsel91';
my $port ='1521';

#connexion à la BD
my $dbd = DBI->connect("dbi:Pg:dbname=$base;host=$hote;port=$port",$login, $mdp)
    	or die 'Connexion impossible à la base de données : '.DBI::errstr;

#parcours du fichier et récupération des motifs (PLM)
while(<PLM>){
	chomp($_);

	#Récupération et écriture de la première ligne
	if($_ =~ m/^motif/){
		my $newline = $_;
		print PLM2 "$newline\n";
	}

	if(!($_ =~ m/^motif/)){
		#Récupération du motif
		(my $Motif,my $info,my $info2,my $info3,my $info4,my $info5,my $info6,my $info7,my $info8,my $info9) = split(/\t/, $_);

		#Execution de la requete sur l'ensemble des informations contenues dans la table Motif
		my $requete = 'SELECT motif_id, name, Is_core, family_place, Description FROM plm.motifs WHERE sequence =\''.$Motif.'\';';

		my $prep = $dbd->prepare($requete)
    		or die 'Impossible de préparer la requête : '.$dbd->errstr;

		$prep->execute
  			or die 'Impossible d\'exécuter la requête : '.$prep->errstr;


		my($motifid, $commentaire, $iscore, $familyplace, $description) = $prep->fetchrow_array;

		$prep->finish;





		#Création de la ligne à entrer
		my $newline = $_."\t$commentaire"."\t$iscore"."\t$familyplace"."\t$description\t";


		#Execution de la requete pour récupérer la source bibliographique

		if($motifid ne ''){
			my $requete_biblio = 'SELECT biblio_ref FROM plm.motif_biblio MB, plm.biblio B WHERE MB.biblio_id=B.biblio_id and motif_id =\''.$motifid.'\';';
			my $prep = $dbd->prepare($requete_biblio)
				or die 'Impossible de préparer la requête : '.$dbd->errstr;

			$prep->execute
  				or die 'Impossible d\'exécuter la requête : '.$prep->errstr;

			while (my @source = $prep->fetchrow_array ) {
    			foreach my $biblio(@source){
        			$newline = $newline.$biblio.' ';
    			}
			}
			$prep->finish;
		}
		my $newline = $newline."\t";


		#Execution de la requete pour récupérer la source bibliographique
		if($motifid ne ''){
			my $requete_organismes = 'SELECT organism FROM plm.motif_species MS, plm.species S WHERE S.species_id=MS.species_id and motif_id =\''.$motifid.'\';';
			my $prep = $dbd->prepare($requete_organismes)
    			or die 'Impossible de préparer la requête : '.$dbd->errstr;

			$prep->execute
  				or die 'Impossible d\'exécuter la requête : '.$prep->errstr;

			while (my @organisme = $prep->fetchrow_array ) {
    			foreach my $orga(@organisme){
        			$newline = $newline.$orga.' ';
    			}
			}
			$prep->finish;
		}

		my $newline = $newline."\n";
		#Ecriture du résultat
		print PLM2 $newline;
	}
}

$dbd->disconnect;
close PLM;

#Suppression du fichier intermédiaire
if(-e $fichier){
	system("rm ".$fichier);
}


close PLM2;

#Renomme le fichier
rename($newfichier,$fichier);
