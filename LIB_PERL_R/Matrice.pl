#!/usr/bin/env perl

use strict;
use warnings;
use Carp;
use Getopt::Long;
use File::Basename;
use File::Find;

# Connaitre d'ou est lancé le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";

#Programme pour réaliser la matrice finale gène/PLM

##################################################################################################################

my %matrice = ();
my $File;
my $name;
my $info;
my $motif;
my @tab;
my $dir;
my %motifs ;
my %PLM_bilan;
my @PLM_tempo;

GetOptions ("dir:s" => \$dir); #$dir étant le répertoire des résultats

if ($dir eq ""){
    print "usage : Matrice.pl
	-dir  <répertoire des résultats>\n";
	exit;
}

find({ wanted => \&process, }, $dir); 

  
sub process { 
	
	my $resdir = $dir."MotifsDInteret/surRepresente";
	if ( -f $File::Find::name ) {

		#Pour récupérer les données des fichiers liste de gènes
		if( $File::Find::name =~ m/\.ListeGenes$/){
			#Récupération du motif
			if($File::Find::name =~ m/\.fasta_/){
				($name,$info) = split(/.fasta_/, $File::Find::name);
			}
			elsif($File::Find::name =~ m/\.fa_/){
				($name,$info) = split(/.fa_/, $File::Find::name);
			}

			@tab = split(/_/, $info);
			$motif = $tab[0];

			
			#Recherche dans le dossier des graphes les PP correspondant au motif recherché (partie pouvant être optimisée)
		    opendir(my $dh, $resdir) || die "Can't open $resdir: $!";
		    while (readdir $dh) {
				my $motif_tmp = "_".$motif."_";
				if($_ =~ m/$motif_tmp/){	
		        	(my $info,my $info2) = split(/Strand_/, $_);
					(my $number,my $motifimage,my $window, my $PP1)  = split(/_/,$info2);
					(my $PP, my $extension) = split(/peak/,$PP1);	
					
					#Stockage de l'ensemble des motifs et leurs positions péférentielles dans la table de hachage (motifs)
					$motifs{$motif}=$PP; 				
				}		        
			}

			closedir $dh;

			#ouverture et traitement d'un fichier motif
			open GENE, "$File::Find::name";
			
			#parcours du fichier et récupération des gènes	
			while(<GENE>){
				chomp($_);
				if(!($_ =~ m/^#/)){

					$matrice{$_}{$motif}=1;
				}
			}
			close GENE;
		}

		#Pour récupérer la liste des PLMs et ne pas afficher tous les motifs sur-représentés
		if($File::Find::name =~ m/ALL$/){

			#ouverture et lecture du fichier bilan avec stockage de la liste des PLMs
			open BILAN, "$File::Find::name";
			
			#parcours du fichier et création de la table contenant tous les PLMs	
			while(<BILAN>){
				chomp($_);
				if(!($_ =~ m/^motif/)){
					@PLM_tempo = split(/\t/,$_);
					$PLM_bilan{$PLM_tempo[0]} = 1;
				}
			}	
			close BILAN;
		}	
	} 
}

#Ecriture de la matrice de présence dans un fichier

my $file = $dir."/PLM_genes.txt";

open(MATRICE,">$file")|| die ("$file $!\n");

#Affichage des motifs (titre des colonnes)
foreach my $titre(sort ({$motifs{$a} <=> $motifs{$b} } keys %motifs)){
	if(exists $PLM_bilan{$titre}){
		print MATRICE "\t$titre\_$motifs{$titre}";
	}
}

print MATRICE " \n";

#Ecriture des genes avec présence ou absence du motif

foreach my $gene (keys %matrice){
	print MATRICE "$gene\t";
	my $number = 0;
	
	foreach my $mot (sort ({$motifs{$a} <=> $motifs{$b} } keys %motifs)){
		if (exists $PLM_bilan{$mot}){
			$number = $number+1;
			if (exists($matrice{$gene}{$mot})){
				print MATRICE "$number\t";
			}
			else{
				print MATRICE "-\t";
			}
		}
	}
	print MATRICE "\n";	
}

close MATRICE;	

