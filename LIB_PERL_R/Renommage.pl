#!/usr/bin/env perl

use strict;
#use warnings;
use Carp;
use Getopt::Long;
use File::Basename;
use File::Find;
use File::Copy qw/ cp move mv /;

# Connaitre d'ou est lancé le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";

#Programme pour renommer les figures produites par PLMdetect ainsi que les fichiers comportant les listes de gènes associées à chaque motif, déplace les fichiers associés aux PLMs dans les répertoires prévus
#Le programme fonctionne sur jacob et non sur bios à cause certainement d'un pb de version ou pb système

##################################################################################################################
#Initialisation des variables
my $dir;
my $fichierBilan;
my $File;
my $info;
my $name;
my $info2;
my $motif; #motif étudiée dans le graphe
my $window; #taille de la fenêtre glissante
my $number; #numéro de process
my $peak; #Position préférentielle
my $newname; #correspond aux noms simplifié des images
my $bmp; #extension .bmp ou .pdf
my @tab;
my %hash;

##################################################################################################################

GetOptions ("dir:s" => \$dir,
	    "fichier:s" => \$fichierBilan); #$dir étant le répertoire des résultats

if ($dir eq ""){
    print "usage : Renommage.pl
	-dir  <répertoire des résultats>
	-fichier <fichierBilan>\n";
	exit;
}

#Création des répertoires PLM_graphs et PLM_lists

mkdir($dir."PLM_graphs/") || die " Problem with the directory $dir/PLM_graphs $!n";
mkdir($dir."PLM_lists/") || die " Problem with the directory $dir/PLM_lists $!n";

#Récupération de tous les PLMs dans une table de hachage pour déplacer les fichiers finaux
open BILAN, $fichierBilan;

while(<BILAN>){
	chomp($_);

	if(!($_ =~ m/^motif/)){
		#Récupération du motif	
		(my $Motif,my $info,my $info2,my $info3,my $info4,my $info5,my $info6,my $info7,my $info8,my $info9) = split(/\t/, $_);
		$hash{$Motif}="";
	}
}

#Renommage
my $dir_rename = $dir."MotifsDInteret/";
find({ wanted => \&process, }, $dir_rename); 

#Les fichiers images initiaux ont des noms sous la forme indiquée dans $test ci dessus, on split ces noms plusieurs fois pour garder uniquement la PP et le motif
  
sub process { 
	if ( -f $File::Find::name ) {
		#Pour renommer les fichiers images bmp
		if( $File::Find::name =~ m/\.bmp$/){

			($info,$info2) = split(/Strand_/, $File::Find::name);

			($number, $motif, $window, $peak)  = split(/_/,$info2);        

			($peak, $bmp) = split(/peak/, $peak);

			$newname = $motif."_".$peak.$bmp;

			my $deplacerG = $File::Find::name;
			my $directionG = $dir."PLM_graphs/".$newname;

			if(exists $hash{$motif}){
				mv( $deplacerG, $directionG ) or die "mv failed: $!";
			}else{rename($File::Find::name, $newname);}
 
		}
		
		#Pour renommer les fichiers images pdf
		if( $File::Find::name =~ m/\.pdf$/){

			($info,$info2) = split(/Strand_/, $File::Find::name);

			($number, $motif, $window, $peak)  = split(/_/,$info2);        

			($peak, $bmp) = split(/peak/, $peak);

			$newname = $motif."_".$peak.$bmp;

			my $deplacerG = $File::Find::name;
			my $directionG = $dir."PLM_graphs/".$newname;

			if(exists $hash{$motif}){
				mv( $deplacerG, $directionG ) or die "mv failed: $!";
			}else{rename($File::Find::name, $newname);}		
		
		}

		#Pour renommer les fichiers liste de gènes pour un motif
		if( $File::Find::name =~ m/\.ListeGenes$/){
			if($File::Find::name =~ m/\.fasta_/){
				($name,$info) = split(/.fasta_/, $File::Find::name);
			}
			elsif($File::Find::name =~ m/\.fa_/){
				($name,$info) = split(/.fa_/, $File::Find::name);
			}			
			@tab = split(/_/, $info);
			$motif = $tab[0];
			$newname = $motif."_listgenes.txt";

			my $deplacerL = $File::Find::name;
			my $directionL = $dir."PLM_lists/".$newname;

			if(exists $hash{$motif}){
				mv( $deplacerL, $directionL ) or die "mv failed: $!"; 
			}else{rename($File::Find::name, $newname);}
			
		}

	} 
}







