package CHEMIN;

## Localisation des programmes utilises pour identifier les motifs d'interet
$LocaProg="./LIB_PERL_R/";

# VARIABLES par defaut pour les recherches

$DEB=-1000;  			# nbr de bases avant le TSS
$FIN=500;    			# 100 (sylvie) nbr de bases max considérées apres le TSS dans l'UTR 5'
$strand="2"; 			# 2 brin etudies par defaut
$APPRENT=500;  		# zone d'apprentissage a partir de DEB
$alphaIC=0.01; 		# IC risque, 1-alphaIC=niveau de confiance
$seuilIC = 1; 		# seuil d'ecart p/r a l'interval de confiance (SMS > seuil)
$Amont=9;     		# zone d'etude autour du TSS, nbr de bases avant le TSS
$Aval=9;					# nbr de bases apres le TSS					
$PasGenome = 1; 	# taille fenetre glissante de depart
$replaceDir="n";	# variable pour remplacement du répertoire
$plotbmp=".bmp"; 		# suffixe des figures
$plotpdf=".pdf"; 		# suffixe des figures
$grad=50;		# pour les graduation sur les graphes (rajout VERO 2016)

$lissage = "";
$form = "pdf";		# correspond au format des graphiques	
$arrondi = "F";		# booléen pour le choix d'arrondir les fenêtres fonctionnelles à 5 bases près ou non ("T" ou "F")	
$UTR = 5;		# correspond à l'UTR que l'utilisateur souhaite étudier (5 ou 3)			
$BD = "y";		# correspond à un booléen indiquant si la recherche des annotations des motifs doit se faire ou non ("y" ou "n")
$doublelissage = "y";	# correspond à la variable indiquant si un double lissage doit être fait (quand un ou des pics secondaires ont un score supérieur à 50% du score du plus grand pic)
$publi = "F";		# correspond à la variable indiquant si les figures doivent être produites en noir et blanc
$thread = 1;		# nombre de coeurs à utiliser par la machine
$enrichment = "";	# fichier référence à utiliser pour le test d'enrichissement, par défaut aucun

# variables pour les fichers de sortie
# on peut rajouter des contraintes pour filtrer que les motifs
# passant ces criteres
# defaut
$NbSeq = 5;   # nb de promoteur min contenant ce motif
@CA = (-9, 9); # position pour le TSS
$LimInfPP = -500; ## min de PP avant TSS
$LimSupPP = 500;  ## max de PP apres TSS
$LimInfL = 1; # taille min fenetre fonctionnelle
$LimSupL = 150;  # taille max fenetre fonctionnelle
$LimSupWind = 100; # taille de la fenetre glissante max
