#!/usr/bin/env perl

use strict;
use Carp;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre d'ou est lanc� le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";
require "$pgmDir/SEQUENCES.pm";

##########################################
#  objectif : generer un fichier qui contient l'ensemble des motifs positionnes
# --> la position d'un motif est determine par la 1ere position de sa sequence
##########################################

my $fTFA;
my $fMotifs;
my $rep;
my $borneDEB = $CHEMIN::DEB;
my $borneFIN = $CHEMIN::FIN;
my $Etude2brins = $CHEMIN::strand;
my $TEMPO;
my $LA1;
##########################################
## On recup les donnees en ligne de commande
GetOptions ("ou=s" => \$TEMPO,
	    "fileTFA=s" => \$fTFA,
	    "fileMot=s" => \$fMotifs,
	    "brin:s" => \$Etude2brins,
      "LA1:s" => \$LA1);

my $BRIN;
if ($Etude2brins ne "$CHEMIN::strand"){
  $BRIN = "+Strand"
}else{
  $BRIN = "+and-Strand"
}

## Verif des donnees
if($fMotifs eq "" || $fTFA eq "" || $TEMPO eq ""){
  print "usage : WordCounter.pl

\tArguments obligatoires :
\t~~~~~~~~~~~~~~~~~~~~~~~~
-fileTFA <fasta file>
-fileMot <file containing the list of motifs to study>
-ou      <directory>

\tArguments facultatifs :
\t~~~~~~~~~~~~~~~~~~~~~~~
-brin    <strand> 2 per default\n\n";
  exit;
}

$borneDEB = $LA1;

## Fichier .tfa -> recup le nom
my $D = rindex($fTFA,"/")+1;
my $EXTENSION = substr($fTFA,$D,length($fTFA));

my $LFB = "ListeFichiersBinvalues";
$LFB = "$LFB$EXTENSION"; ## On rajoute une extension!!
#####################################################################################################
########## LA CREATION DU FICHIER DE COMPTAGE EST A REALISER CAR LE FICHIER EST INEXISTANT ##########
#####################################################################################################

## Lecture fichier sequence multifasta input
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
my %seq;
my $SEQ;
my $nbSeq=0;

open (IN,"$fTFA") || die "can't open $fTFA -> $!\n";
my $warn=0;
while ($_ = <IN>){
	if ($_ =~ /^>(.+)/){
		my($name,@reste)=split(/\s/,$1);
		if($name =~ "_" && $warn==0){print "*** warning: \"_\" remplace par \".\" dans TOUS les noms de sequence***\n";$warn=1}
		$name=~ tr/\_/\./;
		push (@{$seq{$name}}, "");
		if ($SEQ ne ""){
	    push (@{$seq{$SEQ}}, length(${$seq{$SEQ}}[0]));
	    push (@{$seq{$SEQ}}, length(${$seq{$SEQ}}[0]) + $borneDEB);
		}
		$SEQ = $name;
		$nbSeq++;
	}elsif ($_ =~ /^(\w+)$/){
		${$seq{$SEQ}}[0] = "${$seq{$SEQ}}[0]$1";
  }elsif ($_ !~ /^\s/){
		print "WordCounter problem with seq:\n $_ unknown...";exit;
  }
}
close (IN);

## Lecture fichier de motifs -> recup chaque motif dans %motif
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
my %motif;
open (IN2,"$fMotifs") || die "lecture $fMotifs -> $!\n";
while ($_ = <IN2>){
  if ($_ =~ /^(\w+)\s/){
    my $mot = uc($1);
 		# on va verifier si les motifs lus ne contiennet pas trop de N 
 		# un motif doit contenir moins de la moitie de N
    my $lg=length($mot);
    my $lgN=scalar(split("N",$mot)) -1;
    if($lgN < ($lg/2)){
    	$motif{$mot} = length($mot);
    }else{
    	print "warning ### This motif $mot is removed because it is composed of too many N\n";
    }
    
  }
  else { print "warning ### This motif $_ is removed because wrong\n";}
}
close(IN2);

## On etudie la liste de TOUS les motifs pour chercher la pr�sence de chacun dans le fichier fasta
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
my $begin;
my $e;
my $M;
my $c;
my $seqFasta;
my @motiffound = ();
my @motiffound_ = ();
my @motiffound_sorted_ = ();

foreach (keys(%motif)) {
  my $TFA = substr($fTFA,rindex($fTFA,"/")+1,length($fTFA));
  my $FichierCree = "$TEMPO$TFA\_$BRIN\_$_\_relTSS_pas1.binvalues";
  @motiffound = ();
  my $mvb=$_;
  my @LISTE = &SEQUENCES::IUBtoListe($_);
  ## le motif peut etre un code IUB (exemple TATAWA = TATATA ou TATAAA)
  ##-> on traduit en motifs constitues des NT ATGC et on etudie chaque cas :
  foreach $M (@LISTE){
    my $CI = &SEQUENCES::ComplemInv($M);
    my $flag = 0;
    if ($CI eq $M) {$flag = 1;} ## flag = 1 si motif palindromique

    if ($flag == 0 && $Etude2brins eq $CHEMIN::strand) {## etude sur 2 brins
      foreach $seqFasta (keys(%seq)){
				@motiffound_ = ();
	      my $copie = ${$seq{$seqFasta}}[0];
	      # cherche motif sur brin + dans un premier temps
	      #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      	my $brin1 = index(${$seq{$seqFasta}}[0],$M);
	      $begin = - ${$seq{$seqFasta}}[1] + ${$seq{$seqFasta}}[2];
	      while($brin1 != -1){
	        $motiffound_[$#motiffound_+1]{'id_feat_prom'} = $seqFasta;
	        $motiffound_[$#motiffound_]{'begin'} =  $brin1 + $begin;
	        $begin = $motiffound_[$#motiffound_]{'begin'}+1; ## reinitialise $begin pour les prochaines recherchent
	        ${$seq{$seqFasta}}[0] = substr(${$seq{$seqFasta}}[0],$brin1+1,length(${$seq{$seqFasta}}[0])-$brin1+1);
	        $brin1 = index(${$seq{$seqFasta}}[0],$M);
	      }
	      # reinitialise la sequence pour etude brin -
	      ${$seq{$seqFasta}}[0] = $copie;
	
	      # cherche motif sur brin - dans un second temps
      	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	      my $brin2 = index(${$seq{$seqFasta}}[0],$CI);
	      $begin = - ${$seq{$seqFasta}}[1] + ${$seq{$seqFasta}}[2];
	      while($brin2  != -1){	
	       $motiffound_[$#motiffound_ +1]{'id_feat_prom'} = $seqFasta;
	        $motiffound_[$#motiffound_]{'begin'} = $brin2 + $begin + (length($M) -1);
	        $begin = $motiffound_[$#motiffound_]{'begin'}+1-(length($M) -1); ## r�initialise
	        ${$seq{$seqFasta}}[0] = substr(${$seq{$seqFasta}}[0],$brin2+1,length(${$seq{$seqFasta}}[0])-$brin2+1);
	        $brin2 = index(${$seq{$seqFasta}}[0], $CI);
	      }
	
	      ## On met dans l'ordre pour chaque sequence etudiee
	      ${$seq{$seqFasta}}[0] = $copie;
	
	      my @motiffound_sorted_ = sort {$$a{'begin'} <=> $$b{'begin'}} @motiffound_;
	      foreach (@motiffound_sorted_){
	       $motiffound[$#motiffound+1] = $_;
	      }
      }
    }
    else { ##un seul brin
      foreach $seqFasta (keys (%seq)){
	      my $copie = ${$seq{$seqFasta}}[0];
	      my $brin12 = index(${$seq{$seqFasta}}[0],$M);
	      $begin = - ${$seq{$seqFasta}}[1] + ${$seq{$seqFasta}}[2];
	    while($brin12 != -1){	
	     $motiffound[$#motiffound +1]{'id_feat_prom'} = $seqFasta;
	      $motiffound[$#motiffound]{'begin'} =  $brin12 + $begin;
	      $begin = $motiffound[$#motiffound]{'begin'}+1;
	      ${$seq{$seqFasta}}[0] = substr(${$seq{$seqFasta}}[0],$brin12+1,length(${$seq{$seqFasta}}[0])-$brin12+1);
	      $brin12 = index(${$seq{$seqFasta}}[0],$M);
	    }
	    ${$seq{$seqFasta}}[0] = $copie;
      }
    }
  }
  
  ## creation des binvalues
  my @list  =();  ## contient les bornes de debut de pic
  my @list2 =();  ## contient les ID des seq ou les motifs qui ont ete trouves
  foreach $M (@motiffound){
    if($$M{'begin'} >= $borneDEB && $$M{'begin'} <=$borneFIN){
      push(@list,$$M{'begin'});          ## la position de debut de motif
      push(@list2,$$M{'id_feat_prom'});  ## l'ID de g�ne
    }
  }

  
  ## nombre de positions entrant dans l'intervalle du graphe
  my $nbval=scalar(@list);
  ## Analyse en classe pour le graphe
  my @absClass=();
  my $j;
  ## recup toutes les positions a considerer pr la representation en fonction du pas (ex : -1000 -995 ...)
  for($j=$borneDEB; $j <= $borneFIN; $j+=1)
    {push(@absClass,$j);}
  ## nombre d'abscisse a traiter :
  my $nb=scalar(@absClass);
  #initialisation des ordonnees
  my @ordVal=();
  my @ordValNb=();
  ## COMPTAGES
  ##~~~~~~~~~~
  my @ordVal2 = @ordVal;
  my $AQui = $list2[0];
  for(my $i=0; $i < $nbval; $i++) { 
    #parcours de toutes les donnees du fichier
    ## Est ce une nouvelle seq promotrice?
    if($list2[$i] ne $AQui){
      @ordVal2 = @ordVal;
      $AQui = $list2[$i];
    }
    ## Quelle absc augmenter?
    my $borneDEB = -($borneDEB-$list[$i]);
    if ($ordVal[$borneDEB] == $ordVal2[$borneDEB]){
      $ordVal[$borneDEB] = "$ordVal[$borneDEB]$AQui\_";
      $ordValNb[$borneDEB]+=1;
    }
  }
  @ordVal2 = @ordVal;


  ## ECRITURE SORTIE TEXTE
  ##~~~~~~~~~~~~~~~~~~~~~~
  open (OUTTEXT, ">$FichierCree") || die "$FichierCree:$!\n";
  for($j=0;$j<$nb;$j++) {
    print OUTTEXT "$ordVal[$j]\n";  # l'ensemble des genes concatenes
  } 
  close OUTTEXT;

  
  #Ecriture du fichier qui contiendra la liste des binvalues pour prog de R
  open(OUT3,">>$TEMPO/$LFB");
  print OUT3 "$FichierCree\n";
  close (OUT3);
}
