#!/usr/bin/env perl

use strict;
use Carp;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre d'ou est lance le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";
require "$pgmDir/SEQUENCES.pm";

my $borne=-1*$CHEMIN::DEB; #(+1000)
my $de;
my $a;
my $tfa="";
my $fAT="";
my $ID= "no";

my $ou = "";
my $TEMPO_output = "/tmp/PromoterFromFLAGdb.tfa";
my $LA1 = $CHEMIN::DEB;
###################################################
GetOptions ("de=s" => \$de,
	    "a=s" => \$a,
	    "TFA:s" => \$tfa,
	    "AT:s" => \$fAT,
	    "ID:s" => \$ID,
	    "output:s" => \$ou,
      "LA1:s"=> \$LA1);
###################################################

if ($de eq "" || $a eq ""){
    print "usage : fichier.pl
-de  <position de debut souhaitee>
-a   <position de fin souhaitee>
-TFA <fichier fasta> OU 
-AT  <liste des identifiants>
-ID <yes/no>"; # fichier fasta genere
exit;
}

## Fichier tfa a traiter :
if ($tfa ne ""){
	## verification du format du fichier .tfa donne en argument
	if (-e "$tfa" != 1){print "Not found File $tfa\n";exit;}
}else{
	## creation tempo du .tfa associe la liste de motifs
		system ("$CHEMIN::LocaProg/ListeAT2PromSeq.pl -ID $fAT -OUT $TEMPO_output");
		$tfa = $TEMPO_output;
}

## Bornes des sequences a recuperer
if ($de > $a)
{print ("The position $de must be lower than $a\n");exit;}

$borne = -1*$LA1;

###################################################
# lecture des sequences promotrices               #
###################################################

open(IN,"$tfa")|| die "cannot open $tfa:$!";
my $ID_gene;
my %seq_tous;
my $somme=0;
while (<IN>){
  if ($_ =~ /^>(\S+)/){
    $ID_gene = "$1";
    $seq_tous{$ID_gene} = "";
    $somme += 1;
  }elsif ($_ =~ /^(\S+)/){
    $seq_tous{$ID_gene} = "$seq_tous{$ID_gene}$1";
  }
}
close(IN);


open (OUT,">$ou");
foreach (keys (%seq_tous)){
  my $id=$_;
  my $Interet="";

  if(length($seq_tous{$id}) > ($borne+$de)){ ## debut existe?
   if( ($borne+1+$a) <= length($seq_tous{$id}) ){ ## fin existe?
      $Interet = substr($seq_tous{$id},$borne+$de,(1+$a-$de));
    }else{ ## si non, on prend que ce qu'on peut prendre...
      my $tempo = length($seq_tous{$id}) - ($borne+$de);
      $Interet = substr($seq_tous{$id},$borne+$de,$tempo);
    }
    if ($ID eq "yes"){print OUT "$id ";}
    print OUT $Interet."\n";
  }
}
close(OUT);
