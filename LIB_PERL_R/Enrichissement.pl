#!/usr/bin/env perl
use strict;
use Carp;
use Cwd;
use Getopt::Long;
use File::Basename;

my $pgmDir= dirname($0);

# Pgm analyse la sortie du pgm PLMdetect.pl pour chaque motif
# va lancer la recherche du % de promoteurs contenant le motif dans la
# fenetre determiner par le groupe
# va chercher si la fréquence du groupe est significative par arpport à une REF


# Ouverture du répertoire source contenant le genome
my $genome="";
my $dir;
my $name;
my $bilan;

my $valoptionDefault="\n*** Parameters ***\n
-refFile --> File containing all promoter sequences from the reference genome - default $genome
-dir --> Repository where the Bilan file is available

____________________________________________________________________________\n \n";



# Récupération des options
GetOptions (
	    "refFile:s" => \$genome,
	    "dir:s" => \$dir,
	    "bilan:s" => \$bilan);

if($dir eq ""){
	print "INPUT ERROR : Enrichissement_auto a directory name where the Bilan file is available\n\n";
	exit;
}

if($genome ne "" && !(-e $genome)){
	print "INPUT ERROR : argument refFile doesn't exist, give the reference genome file (only promoters) \n\n";
	exit;
}

if($bilan eq "" || !(-e $bilan)){
	print "INPUT ERROR : argument bilan doesn't exist, give the bilan file \n\n";
	exit;
}


if($genome eq "" && $dir eq ""){
	print "usage :  Enrichissement.pl parameters\n $valoptionDefault\n";
	exit;
}

if($genome ne ""){
	$dir = $dir."/";
	my @path_genome = split(/\//,$genome);
	my $file = $path_genome[$#path_genome]; 
	my %table_enrichissement;

	# le fichier bilan
	my $alpha=0.05; # valeur test max
	my $nbREF = `grep -c '>' $genome`;
	chomp($nbREF);

	# Repertoire de travail = repertoire ou le pgm chercher le fichier BilanSEMMsup1.recup_ALL
	opendir(EXPL,$dir)|| die "Impossible d'ouvrir le répertoire source";

	while (my $fichier = readdir(EXPL)){
		if ($fichier =~ "BilanSEMMsup1.recup_ALL"){
			open(IN,"$dir/$fichier") || die "pb pour ouvrir $dir/$fichier\n";
			my $line=<IN>;
			while($line=<IN>){
				my($motif,$w,$score,$pp,$deb,$fin,$succesGroupe,$pourcentGroupe,$nbGroupe,$reste)=split(/\s+/,$line);

				# Calcul du nbr de sequences ayant le motif - Appel a GenereListeAT.pl
				my $com="perl ".$pgmDir."/GenereListeAT.pl --brin 2 --motif $motif --D $deb --F $fin --adresse $dir --TFA $genome";
				system("$com");

				# Recupeartion du nb de promoteurs dans le genome + nb avec motif
				my $f="$dir/".$file."_".$motif."_".$deb."_".$fin."_*.ListeGenes";
				open(PROBA,"grep sequences $f|");
				my $val=<PROBA>;
				$val=~ "([0-9]+) sequences";
				my $succesREF=$1;
				my @info_tempo = ($succesREF, $nbREF, $succesGroupe, $nbGroupe);

				# Stockage dans une table de hachage
				$table_enrichissement{$motif}=\@info_tempo;
			
				# Suppression du fichier Liste de gène intermédiaire
				system("rm ".$f);
			}
		} 
	} 

	open(TABLE, ">".$dir."table_enrichissement.txt");
	foreach my $motif (keys %table_enrichissement){
		print TABLE "$motif\t$table_enrichissement{$motif}[0]\t$table_enrichissement{$motif}[1]\t$table_enrichissement{$motif}[2]\t$table_enrichissement{$motif}[3]\n";
	}
	close TABLE;

	# Ecriture dans un fichier l'exécutable R et obtention du fichier d'enrichissement
	open(PGR,">ex.R")|| die "pb ouverture fichier $!\n";
	print PGR "source(\"".$pgmDir."/HypergeometricTest.R\")\n";
	print PGR "TestHyperGeoForPLM(\"".$dir."table_enrichissement.txt\", \"".$dir."\",\"".$bilan."\")\n q()";
	close(PGR);
	system("R --silent --no-save < ex.R > tempo_enrichissement");

	system("rm ".$dir."table_enrichissement.txt");
}else{
	# Ecriture dans un fichier l'exécutable R et obtention du fichier d'enrichissement
	open(PGR,">ex.R")|| die "pb ouverture fichier $!\n";
	print PGR "source(\"".$pgmDir."/HypergeometricTest.R\")\n";
	print PGR "TestHyperGeoForPLM(namefile = \"\", \"".$dir."\",\"".$bilan."\")\n q()";
	close(PGR);
	system("R --silent --no-save < ex.R > tempo_enrichissement");

}
	
system("rm tempo_enrichissement");
system("rm ex.R");
