#!/usr/bin/env perl

use strict;
use Carp;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;

# Connaitre d'ou est lance le pgm
my $pgmDir=dirname($0);
require "$pgmDir/CHEMIN.pm";

# pgm de mises en forme des resultat de motifs

##############################################################################################################
## fichiers et repertoires donnes par l'utilisateur
my $RepertoireR;
my $Motif;
my $SEMM;
my $OUT;
my $format = $CHEMIN::form;

## options
my $NbSeq = $CHEMIN::NbSeq;   # nb de promoteur min contenat ce motif
my $LimInfPP = $CHEMIN::DEB + $CHEMIN::APPRENT; ## min de PP avant TSS
my $LimSupPP = $CHEMIN::LimSupPP;  ## max de PP apres TSS
my $LimInfL = $CHEMIN::LimInfL; # taille min fenetere fonctionnelle
my $LimSupL = $CHEMIN::LimSupL;  # taille max fenetre fonctionnelle
my $LimSupWind = $CHEMIN::LimSupWind; # taille de la fenetre glissante max
my $lg;

##############################################################################################################
GetOptions ("Rep=s" => \$RepertoireR,
	    "SEMM=s" => \$SEMM,
	    "out=s" => \$OUT,
	    "nbSeq=s" => \$NbSeq,
	    "lg=s" => \$lg,
	    "W=s" => \$LimSupWind,
	    "format=s" => \$format,
      "LimInfPP=s" => \$LimInfPP);
##############################################################################################################

# entete des colonnes

my $LISTCOL="motif	winUsed	ScoreSMS	PP	functionalWin	nbGenes	%	nbPromos	sizeFuncWin	function	is_core	family_place	description	biblio	organisms\n";


if ($RepertoireR eq "" || $SEMM eq "" || $OUT eq ""){
  print "usage :
fichier.pl
-Rep <Nom du repertoire ou sont les plot>
-SEMM <SEMM a considerer -> 3 si on veut > 3IC>
-out <fichier de sortie>
-format <format des fichiers graphiques>
options :
[-nbSeq <Nombre de sequences mini contenant un motif M> --> par defaut $NbSeq]
[-lg    <Longueur des motifs a recuperer                --> par defaut TOUTES]
[-W     <Longueur maximale de fenetre glissante> --> par defaut $LimSupWind]
\n";
  exit;
}

my $Repertoire = $RepertoireR;
my @jpg;
my @jpg2;

opendir REP,"$Repertoire";

if($format eq "bmp"){
	 @jpg = grep /$CHEMIN::plotbmp$/, readdir REP;
}elsif($format eq "pdf"){
	 @jpg = grep /$CHEMIN::plotpdf$/, readdir REP;
}

closedir REP;

opendir REP,"$Repertoire\/CentreSur0/";
if($format eq "bmp"){
	 @jpg2 = grep /$CHEMIN::plotbmp$/, readdir REP;
}elsif($format eq "pdf"){
	 @jpg2 = grep /$CHEMIN::plotpdf$/, readdir REP;
}


closedir REP;
@jpg = (@jpg,@jpg2);

my %MotifsInteret;
my $fichier;

foreach $fichier (@jpg){
  if($fichier =~  m/_(\d+\.?\d*)_(\w+)_Windows(\d+)_(-?\d+\.?\d*)peak/){
    if($1 >= $SEMM){
      push(@{$MotifsInteret{$2}},$3);
      push(@{$MotifsInteret{$2}},$1);
      my $deux = $2;
      my $trois = $4;
      push(@{$MotifsInteret{$deux}},$trois);
		}
  }elsif($fichier =~  m/_Inf_(\w+)_Windows(\d+)_(-?\d+\.?\d*)peak/){
    ## cas ou pdt apprentissage -> seuil IC = 0
    push(@{$MotifsInteret{$1}},$2);
    push(@{$MotifsInteret{$1}},"99.99");
    my $deux = $3;
    my $un = $1;
    push(@{$MotifsInteret{$un}},$deux);
  }
}

my $ListeMI = "/ListeMotifsInteret/";
my $Rep2 = "$Repertoire$ListeMI";

opendir REP,"$Rep2";
my @fichier = grep /ListeGenes/, readdir REP;
closedir REP;

my $Motif;
my $f;

foreach $Motif (keys(%MotifsInteret)){
  foreach $f (@fichier){
    if ($f =~ m/_$Motif\_(-?\d*)_(-?\d*)_/){
      push(@{$MotifsInteret{$Motif}},$1);
      push(@{$MotifsInteret{$Motif}},$2);
      open(IN,"$Rep2$f");
      while(<IN>){
	      if($_ =~ /^#(\d*) sequences/){ 
	        push(@{$MotifsInteret{$Motif}},$1);
	      }
	      if($_ =~ /^#(\d*.?\d*)% of the sequences in promoter set with.+set with (\d*) sequences/){
	        push(@{$MotifsInteret{$Motif}},sprintf("%.2f",$1));
	        push(@{$MotifsInteret{$Motif}},$2);
	      }elsif($_ =~ /^#(\d*.?\d*)% of the sequences.+/){ 
	        push(@{$MotifsInteret{$Motif}},sprintf("%.2f",$1));	
        }
      }
      close(IN)
    }
  }
}

## Tri par ordre decroissant en fonction de la 1ere valeur du tableau de la table de HASH --> le SEMM
# ecriture fichier principale + entete
open(SORTIE,">$OUT\_ALL");
print SORTIE $LISTCOL;

foreach $Motif (sort{$MotifsInteret{$b}[1] cmp $MotifsInteret{$a}[1]} keys %MotifsInteret){
  if (length($Motif) == $lg || $lg eq ""){
    if (${$MotifsInteret{$Motif}}[5] >= $NbSeq     ## Nb seq OK avec souhait utilisateur
    && ${$MotifsInteret{$Motif}}[0] <= $LimSupWind   ## Taille maxi de fenetre non atteinte
    && ${$MotifsInteret{$Motif}}[2]  >= $LimInfPP    ## Posi prefe dans region OK avec boxplot
    && ${$MotifsInteret{$Motif}}[2]  <= $LimSupPP 
    && (${$MotifsInteret{$Motif}}[4]-${$MotifsInteret{$Motif}}[3]+1)  <= $LimSupL ## Largeur OK avec boxplot
    && (${$MotifsInteret{$Motif}}[4]-${$MotifsInteret{$Motif}}[3]+1)  >= $LimInfL){
      my $Largeur = 1 + ${$MotifsInteret{$Motif}}[4] - ${$MotifsInteret{$Motif}}[3];
      if (${$MotifsInteret{$Motif}}[3] >= 0){${$MotifsInteret{$Motif}}[3] += 1;}
      if (${$MotifsInteret{$Motif}}[4] >= 0){${$MotifsInteret{$Motif}}[4] += 1;}

      print SORTIE "$Motif";
      for (my $i = 0; $i<=7; $i++){
        print  SORTIE "\t${$MotifsInteret{$Motif}}[$i]";
      }

      print SORTIE "\t$Largeur\n";
    }
  }
}

close(SORTIE);
