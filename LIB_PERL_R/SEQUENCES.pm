package SEQUENCES;

my $TsLesIUB = "RYMKBVHDWSNATGC";

sub maxTab { my $max = shift; $_ > $max and $max = $_ for @_; $max }
sub minTab { my $min = shift; $_ < $min and $min = $_ for @_; $min }
sub IndMin{
    my @T = (@_);
    my $ind = 0;
    for ($i = 1; $i <= $#T; $i++){
	if($T[$i] <= $T[$ind]){
	    $ind = $i;
	}
    }
    $ind
}


sub RecupNomFichier(){
    my ($F1)=($_[0]);
    my $d =  rindex($F1,"/")+1;
    my $f =  rindex($F1,".")-1;
    if($f == -1){$f = length($F1);}
    my $f1 = substr($F1,$d,$f-$d+1);
    $f1;
}

sub NbL(){
    my ($file)=($_[0]);
    my ($res)=0;
    open(FIC,"$file") or die("can't open $file : $!");
    while (<FIC>) { $res++; }
    close(FIC);
    $res;
}

### Prend un vecteur (sequence IUB ok) et retourne le complement inverse
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub ComplemInv()  {
  my ($motif)=($_[0]);
  $motif =~ tr /RrYyMmKkBbVvHhDdWwSsNATGCatgc/YyRrKkMmVvBbDdHhWwSsNTACGtacg/;
  my $CI = reverse($motif);
  return $CI;
}

### Prend un vecteur (sequence NT) et retourne un tableau avec toutes les mutations possibles d'une base
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub ListeDesMutations(){
  my ($motif)=($_[0]);
  my @decoupe = split(//,$motif);
  my @liste;
  foreach(@decoupe){
    if ($_ eq "A")
      {$_ = "T"; $liste[$#liste+1] = join("",@decoupe);
       $_ = "G"; $liste[$#liste+1] = join("",@decoupe);
       $_ = "C"; $liste[$#liste+1] = join("",@decoupe);
       $_ = "A";} ## Retour  la version initiale
    else{
      if ($_ eq "T")
	{$_ = "A"; $liste[$#liste+1] = join("",@decoupe);
	 $_ = "G"; $liste[$#liste+1] = join("",@decoupe);
	 $_ = "C"; $liste[$#liste+1] = join("",@decoupe);
	 $_ = "T";} ## Retour  la version initiale
      else{
	if ($_ eq "C")
	  {$_ = "T"; $liste[$#liste+1] = join("",@decoupe);
	   $_ = "G"; $liste[$#liste+1] = join("",@decoupe);
	   $_ = "A"; $liste[$#liste+1] = join("",@decoupe);
	   $_ = "C";} ## Retour  la version initiale
	else
	  {$_ = "T"; $liste[$#liste+1] = join("",@decoupe);
	   $_ = "C"; $liste[$#liste+1] = join("",@decoupe);
	   $_ = "A"; $liste[$#liste+1] = join("",@decoupe);
	   $_ = "G";} ## Retour  la version initiale
	
      }
    }
  }
  @liste;
}

### Prend un vecteur (sequence NT) et retourne un booleen -> =1:motif palindromique =0:motif non palindr
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


sub EstUnPalindrome(){
  my ($motif) = ($_[0]);
  my $Palind = 0;
  my $complem = $motif;
  $complem =~ tr /RrYyMmKkBbVvHhDdWwSsNATGCatgc/YyRrKkMmVvBbDdHhWwSsNTACGtacg/;
  my $complem_inv = reverse($complem);
  if($complem_inv eq $motif){
    $Palind = 1; ## palindrome trouve
  }
  $Palind;
}

### Prend un vecteur (sequence NT) et inverse les lettres en minusc par des lettres en maj et inversement
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub MinMaj(){
  my ($motif)=($_[0]);
  $motif =~ tr /ATGCatgc/atgcATGC/;
  $motif;
}


###########################################################################################################
###########################################################################################################
###########################################################################################################



### Prend un vecteur (sequence de NT avec (A/T) si veut W code IUB) et retourne le code IUB correspondant
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub SeqChoixToIUB(){
  my ($seq)=($_[0]);
  my $Res="";
  ## isole les parties avec plrs bases possibles
  my @decoupe = split(/[(,)]/,$seq);
  foreach(@decoupe){
    if ($_ ne ""){
      if ($_ =~m /\//){
	my @lettres =  split(/\//,$_);
	@lettres = sort (@lettres);
	$_=join("",@lettres);
	## Transfo des choix possibles en code IUB
	$_ = toIUBcode($_);
      }
      $Res= "$Res$_";
    }
  }
  $Res;
}

sub toIUBcode(){
  my ($code)=($_[0]);
  $code= lc($code);
  if ($code =~ /[^acgt]/){print "Erreur toIUBcode() -> base(s) differente(s) de ACGT\n";exit;}
  my @lettres =  split(//,$code);
  @lettres = sort (@lettres);
  $code=join("",@lettres);
  my $res;
  if ($code eq "acgt"){$res = "N";}
  elsif ($code eq "ag"){$res = "R";}
  elsif ($code eq "ac"){$res = "M";}
  elsif ($code eq "at"){$res = "W";}
  elsif ($code eq "cg"){$res = "S";}
  elsif ($code eq "ct"){$res = "Y";}
  elsif ($code eq "gt"){$res = "K";}
  elsif ($code eq "cgt"){$res = "B";}
  elsif ($code eq "agt"){$res = "D";}
  elsif ($code eq "act"){$res = "H";}
  $res;
}

###########################################################################################################
###########################################################################################################
###########################################################################################################

sub Match(){
  my ($M1)=($_[0]);
  my ($M2)=($_[1]);
  my $matchMax = 0;
  my @M1 = IUBtoListe($M1);
  my @M2 = IUBtoListe($M2);
  my $c1;
  my $c2;
  my $CptRef = 0; 
  my $CptDecal;
  my $tempoI;
  my $tempoJ;
  foreach $c1 (@M1){
      my @L1 =  split(//,$c1);
      foreach $c2 (@M2){
	  my @L2 =  split(//,$c2);
	  my $REFmax = $#L2;
	  if ($#L1 > $REFmax){
	      $REFmax = $#L1;
	  }
	  for(my $i = 0; $i<=$#L1; $i++){	
	      for(my $j = 0; $j<=$#L2; $j++){
		  my $CptTempo = 0;
		 
		  #print "Remise a 0\n";
		  for (my $k = 0; $k <= $REFmax; $k++){ ## toutes les positions possibles
		      if ($i+$k <= $#L1 && $j+$k <= $#L2){
			  #print $i+$k,"  ",$j+$k,"\n";
			  if ($L1[$i+$k] eq $L2[$j+$k]){
			      $CptTempo+=1;
			      $tempoI = $i;
			      $tempoJ = $j
			  }

		      }
		  }
		  #print "$CptTempo Meilleur Que $CptRef?\n";
		  if ($CptRef < $CptTempo){
		      $CptRef = $CptTempo;
		      $CptDecal = $tempoI-$tempoJ;
		  }
	      }
	  }
      }
  }
  my @renvoi= ($CptRef,$CptDecal);
  @renvoi;
}

### Prend un vecteur (sequence d'ADN avec code IUB) et retourne le "code choix": 
### ex: Motif CATW -> 11120
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub IUBtoCode(){
    my ($mot)=($_[0]);
    my @decoupe = split(//,$mot);
    my $code;
    foreach(@decoupe){
	if($_ =~ /[ACGTacgt]/){$code=$code."1";}
	elsif($_ =~ /[Nn]/){$code=$code."4";}
	elsif($_ =~ /[BDHVbdhv]/){$code=$code."3";}
	elsif($_ =~ /[RYWMKSrywmks]/){$code=$code."2";}
    }
    $code;
}


### Prend un vecteur (sequence d'ADN avec code IUB) et retourne la liste des sequences possibles
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
sub IUBtoListe(){
  my ($mot)=($_[0]);
  ## Cas ou le motif n'est consitu que des bases A T G ou C
  my @ToutesSolutions;
  if ($mot !~ /([^ATGCatgc])/)
    {$ToutesSolutions[0] = $mot;}
  ## Cas ou un motif dgnr est donn en argument
  else{
    @ToutesSolutions=ATGC($mot);
    my @ToutesSol2;
    my @vide;
    my @Fin = ATGC($ToutesSolutions[0]);
    while ($#Fin > 0){
      @ToutesSol2 = @vide;
      foreach(@ToutesSolutions){
	@Fin = ATGC($_);
	@ToutesSol2 = (@ToutesSol2,@Fin);
      }
      @ToutesSolutions = @ToutesSol2;
      @Fin =  ATGC($Fin[0]);
    }
  }
  @ToutesSolutions;
}

sub ATGC(){
  my ($motif) = ($_[0]);
  my @liste;
  if ($motif =~ /([^ATGCatgc])/){
    my $KO = $1;
    my @choix=TraductionBases($1);
    foreach  (@choix){
      $liste[$#liste+1] = $motif;
      $liste[$#liste] =~ s/$KO/$_/;
    }
  }
  @liste;
}

###########################################################################################################
###########################################################################################################
###########################################################################################################
### Prend un code IUB et retourne la liste des NT possibles
###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

sub TraductionBases(){
  my $code=$_[0];
  $code= lc($code);
  if ($code =~ /[^acgturymkwsbdhvn]/){print "Erreur TraductionBases() -> pas que code IUB\n";exit;}
  my @res;
  if ($code eq "a"){$res[0] = "A";}
  elsif ($code eq "t"){$res[0] = "T";}
  elsif ($code eq "c"){$res[0] = "C";}
  elsif ($code eq "g"){$res[0] = "G";}
  elsif ($code eq "n"){$res[0] = "A";$res[1] = "C";$res[2] = "G";$res[3] = "T";}
  elsif ($code eq "r"){$res[0] = "A";$res[1] = "G";}
  elsif ($code eq "y"){$res[0] = "C";$res[1] = "T";}
  elsif ($code eq "m"){$res[0] = "C";$res[1] = "A";}
  elsif ($code eq "k"){$res[0] = "G";$res[1] = "T";}
  elsif ($code eq "w"){$res[0] = "A";$res[1] = "T";}
  elsif ($code eq "s"){$res[0] = "C";$res[1] = "G";}
  elsif ($code eq "b"){$res[0] = "C";$res[1] = "G";$res[2] = "T";}
  elsif ($code eq "d"){$res[0] = "A";$res[1] = "G";$res[2] = "T";}
  elsif ($code eq "h"){$res[0] = "A";$res[1] = "C";$res[2] = "T";}
  elsif ($code eq "v"){$res[0] = "A";$res[1] = "C";$res[2] = "G";}
  @res;
}

## donne en argu tablo + 1  n argu en plus
## fait le uniq de a... si toujours mme nombre de donnes, 
## l'argu / les argu etaient inclus dans le tableau
## sinon, non
sub EstInclu {
    my (@liste) = @_;
    my @U = uniq(@liste);
    my $flag = "no";

    if($#U < $#liste){
	$flag="yes";
    }
    $flag;
}
## avec 2 noms de fichiers... ecrit dans F1 tout ce qui est inclu
## dans F1 ET AUSSI dans F2
sub doublons {
  my (@liste) = @_;
  my $f1 = $liste[0];
  my $f2 = $liste[1];
  system("cat $f2 > /tmp/V$$");
  system("cat $f1 >> /tmp/V$$");
  system("sort /tmp/V$$ > /tmp/VV$$");
  system("uniq -d /tmp/VV$$ > $f1");
  system("rm /tmp/V$$ /tmp/VV$$");
}

sub uniq {
  my (@liste) = @_;
  my %deja_vu;
  my @uniq;
  foreach(@liste){
    unless ($deja_vu{$_}){
      $deja_vu{$_} = 1;
      push(@uniq,$_);
    }
  }
  @uniq;
}

sub uniqSANSci {
  my (@liste) = @_;
  my %deja_vu;
  my @uniq;
  foreach(@liste){
    unless ($deja_vu{$_}){
      $deja_vu{$_} = 1;
      my $CI = $_;
      $CI =~ tr /RrYyMmKkBbVvHhDdWwSsNATGCatgc/YyRrKkMmVvBbDdHhWwSsNTACGtacg/;
      $CI = reverse($CI);
#print "$CI -> $_\n";
      $deja_vu{$CI} = 1;
      push(@uniq,$_);
    }
  }
  @uniq;
}

sub estIUB {
  my ($seq) = ($_[0]);
  $seq = uc($seq);
  if($seq !~ /^[ACGTRYMKWSBDHVN]+$/){return 0;}
  return 1;
}
###########################################################################################################
###########################################################################################################
###########################################################################################################
1;

