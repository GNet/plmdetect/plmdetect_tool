#!/usr/bin/env perl

use strict;
use Carp;
use Cwd;
use Getopt::Long;
Getopt::Long::Configure('prefix=--');
use File::Basename;
use Parallel::ForkManager;

##################################################################################################################################################################################################################################
#L'objectif du programme est de pseudo-paralléliser le processus demandé par l'utilisateur, pour cela le fichier de motif est découpé en n fois, n étant le nombre de threads choisit par l'utilisateur et on lance le processus # en même temps pour tous les fichiers créés et on regroupe dans le même répertoire de sortie 																	 #
##################################################################################################################################################################################################################################

# Connaitre ou est le pgm
my $pgmDir=dirname($0)."/LIB_PERL_R";
require "$pgmDir/CHEMIN.pm";

# parametres
my $fSeq; # name parameter -seqFile
my $fSeq_doc="--seqFile a file containing the promoters (1000 bases before TSS/TTS and 500 bases after) to study in fasta format\n";
my $fMotif; # name parameter -motifFile
my $fMotif_doc="--motifFile a file containing a list of motifs to study (see also generateMotif.pl)\n";
my $dir; # name parameter -dir
my $dir_doc="--dir a working directory name to output all the results\n";
my $Date = localtime; #date

## parametres du graphe defauts
my $alpha   = $CHEMIN::alphaIC; # risque pour Intervalle de Confiance
my $seuilIC = $CHEMIN::seuilIC; # seuil d'ecart a lintervalle de Confiance (1 fois, 2 fois etc...))


my $brin=$CHEMIN::strand;
my $replaceDir = $CHEMIN::replaceDir; #variable pour remplacer le répertoire portant déjà le nom
my $lissage = $CHEMIN::lissage; #variable indiquant la valeur du lissage (taille de la fenêtre glissante) pour les graphiques (par défaut fenêtre croissante)
my $format = $CHEMIN::form; #variable définissant le format des graphiques en sortie de R
my $arrondi = $CHEMIN::arrondi; #booléen pour arrondir à 5 bases près les fenêtres fonctionnelles
my $UTR = $CHEMIN::UTR; #3 ou 5 correspondant à la région que l'on souhaite étudier
my $BD = $CHEMIN::BD; #y ou n pour indiquer si la recherche des annotations doit se faire dans la BD
my $doublelissage = $CHEMIN::doublelissage; #y ou n pour décider d'appliquer le double lissage ou non
my $publi = $CHEMIN::publi; #T ou F pour indiquer si les figures sont à destinations d'une publication (rendu en noir et blanc)
my $thread = $CHEMIN::thread; #nombre de coeurs à utiliser
my $refFile = $CHEMIN::enrichment; #fichier référence pour l'enrichissement
my $LA1 = $CHEMIN::DEB;
my $LA2 = $CHEMIN::DEB + $CHEMIN::APPRENT;
my $LimInfPP = $LA2; 

# messages et controles des parametres
my $entete = "\n\tYou don't submit all mandatory parameters\n\t~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
my $entete2 = "\n\tError in optional parameters \n\t~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";


GetOptions (
	    "seqFile:s" => \$fSeq,
	    "motifFile:s" => \$fMotif,
	    "dir=s" => \$dir,
	    "alpha:s" => \$alpha,
	    "strand:s" => \$brin,
	    "scoreIC:s"=> \$seuilIC,
	    "replaceDir:s"=>\$replaceDir,
	    "slidingWindow:s"=>\$lissage,
	    "format:s"=>\$format,
	    "round:s"=>\$arrondi,
	    "UTR:s"=>\$UTR,
	    "annotation:s"=>\$BD,
	    "smooth:s"=>\$doublelissage,
	    "publi:s"=>\$publi,
	    "thread:i" => \$thread,
	    "enrichment:s" => \$refFile,
		"LA1:s" => \$LA1,
		"LA2:s" => \$LA2);

my $valParameters="*** Mandatory parameters ***\n \t$fSeq_doc \t$fMotif_doc \t$dir_doc";
my $valoptionDefault="\n*** Optional Parameters default used: ***\n
--alpha   --> $alpha the level of risk, (1-alpha) is the confidence level (default $CHEMIN::alphaIC)
--strand  --> One or Two strands (default 2 strands)
--scoreIC --> $seuilIC the number of deviance from confidence interval to consider PLM (default $CHEMIN::seuilIC)
--replaceDir --> With y if the directory already exists, delete it and replace by a new one with the same name 
		With n if the directory already exists, rename the new directory by adding \"_1\" (default $CHEMIN::replaceDir)
--slidingWindow --> Define the size of the sliding window for graphics, an integer between 1 and 100 (default increasing size)  
--format --> Graphics format : bmp or pdf (default $CHEMIN::form)
--round --> Boolean (T or F) for rounding functionnal window values (5 bases) - default $CHEMIN::arrondi
--UTR --> Choose the region which will be analysed (5 or 3) - default $CHEMIN::UTR
--annotation --> Add functionnal annotation to each motif contained in database (y or n) - default $CHEMIN::BD
--smooth --> Double smoothing for the graphics (when other peak scores are higher than 50% of the maximum peak) (y or n) - default $CHEMIN::doublelissage
--publi --> Produces graphics in black and white (T or F) - default $CHEMIN::publi
--thread --> Number of threads to use (integer) - default $CHEMIN::thread
--enrichment --> File containing all promoters which are considered as reference for hypergeometric test  - default none enrichment done
--LA1 --> First boudary of the learning area, it corresponds to the first coordinate of the sequences - default $CHEMIN::DEB
--LA2 --> Second boudary of the learning area, it corresponds to the first coordinate of the studied region - default $CHEMIN::DEB+$CHEMIN::APPRENT

____________________________________________________________________________\n \n"; #Ligne pour la mise en forme des résultats (sépare les options et le reste des résultats

###########################################################################################################################################################

################################
### Vérification des arguments #
################################

print "usage :  PLMdetect.pl parameters\n $valParameters $valoptionDefault\n";


##Vérifications
if($fMotif eq "" || !(-e $fMotif)){
	print "$entete enter a name of file of motifs (parameter motifFile) or $fMotif does not exist\n";
  	exit;
}
if($fSeq eq "" || !(-e $fSeq)){
	print "$entete enter a name of file of promoters (parameter seqFile) or $fSeq does not exist\n";
  	exit;
}

#le nom doit contenir l'extension .fasta ou .fa
if(!($fSeq =~ m/\.fa$/)&&($fSeq =~ m/\.fasta$/)&&($fSeq =~ /\.fa$/m)&&($fSeq =~ /\.fasta$/m)){
	print "INPUT ERROR : The sequence file needs a .fasta or .fa extension, just rename the file \n\n";
	exit;
}

#le nom du répertoire est bien spécifié
if($dir eq ""){
	print "INPUT ERROR : PLMdetect needs a directory name \n\n";
	exit;
}

#Le paramètre de publication est bien conforme
if($publi ne "T" && $publi ne "F"){
	print "INPUT ERROR : \"publi\" has to be T (true) or F (false) \n\n";
	exit;
}

#Vérifie que le paramètre de fenetre glissante entré est bien conforme (un entier compris entre 1 et 100)
if($lissage ne ""){
	if( ! ( $lissage =~ m/^\d*$/ ) ){
		print "INPUT ERROR : The size of the sliding window has to be an integer \n \n";
		exit;
	}
	if($lissage < 1 || $lissage > 100){
		print "INPUT ERROR : The size of the sliding window has to be between 1 and 100 \n \n";
		exit;	
	}
}

#Vérifie que le paramètre de format de fichier est conforme (pdf ou bmp)
if($format ne "bmp" && $format ne "pdf"){
	print "INPUT ERROR : Format can be only pdf or bmp \n \n";
	exit;
}

#Vérifie que le paramètre round est bien T ou F
if($arrondi ne "T" && $arrondi ne "F"){
	print "INPUT ERROR : Round has to be T (true) or F (false)\n\n";
	exit;
}

#Vérifie que le paramètre UTR est bien 5 ou 3
if($UTR != 5 && $UTR != 3){
	print "INPUT ERROR : UTR has to be 5 (5' UTR) or 3 (3' UTR)\n\n";
	exit;
}

#Vérifie que le annotation est conforme
if($BD ne "y" && $BD ne "n"){
	print "INPUT ERROR : annotation parameter has to be y (yes) or n (no)\n\n";
	exit;
}

#Vérifie que le paramètre smooth est conforme
if($doublelissage ne "y" && $doublelissage ne "n"){
	print "INPUT ERROR : smooth parameter has to be y (yes) or n (no)\n\n";
	exit;
}

#Vérifie que si un double lissage est demandé et le paramètre de lissage a été fixé, le programme renvoie une erreur car contradictoire
if($doublelissage eq "y" && $lissage ne ""){
	print "CONTRADICTORY INPUTS : Double smoothing is not possible when the slinding window size is fixed\n\n";
	exit;
}

#Vérifie que le nombre de threads donné en entrée est correct
if($thread <= 0 || int($thread) != $thread){
	print "INPUT ERROR : Argument thread has to be an integer > 0\n\n";
	exit;
}

#Vérifie que le paramètre enrichment est conforme
if($refFile ne "" && !(-e $refFile)){
	print "INPUT ERROR : Enrichment reference file doesn't exist\n\n";
	exit;
}

#Vérifie que les paramètres de région d'apprentissage sont conformes
if(!( $LA1 =~ m/[+-]\d*$/) || !($LA2 =~ m/[+-]\d*$/)){
		print "INPUT ERROR : LA1 and LA2 have to be intergers \n \n";
		exit;
}

if($LA1 > $LA2){
	print "INPUT ERROR : First bondary of learning area is bigger than the second\n\n";
	exit;
}

$LimInfPP = $LA2;

###########################################################################################################################################################
#création du répertoire de travail si un remplacement est demandé
if($replaceDir eq "y"){
	if(-e $dir){
		system("rm -R ".$dir);
	}

	mkdir($dir) || die "Problem with the directory $dir $!\n";
}

#création du répertoire de travail si un remplacement n'est pas demandé
my $nbDir = 1;
if($replaceDir eq "n"){
	if(-e $dir){
		my $dir_inter = $dir."_".$nbDir;
		while(-e $dir_inter){
			$nbDir = $nbDir+1;
			$dir_inter = $dir."_".$nbDir;			
		}
		$dir = $dir_inter;
	}
	mkdir($dir) || die "Problem with the directory $dir $!\n";
}

print "Result repository : ".$dir."\n";

#Création du fichier de log
my $filelog=$dir."/PLMdetect.log";
open(LOG,">$filelog")|| die ("$filelog $!\n");;

print LOG "usage :  PLMdetect.pl parameters\n $valParameters $valoptionDefault\n";
print LOG "Result repository: $dir";

my $Msur = 0;
## On verifie qu'il n'y a bien QUE des motifs
open(IN,"$fMotif") || die ("$fMotif: $!\n");
while(my $M=<IN>){
	$M = uc($M);
  	chomp($M);
  	$M=~ s/\s//;
	$Msur++;
}

close(OUT);
close(IN);

my $nbSeq;

open SEQUENCES, $fSeq;
while(<SEQUENCES>){
	chomp($_);
	if($_ =~ m/^>/){
		$nbSeq++;
	}
}
close SEQUENCES;

print "\n****************************\n";
print "Analyze is running...\n";
print  "Motifs : $Msur motifs read \n";
print "$nbSeq sequences read \n";

print LOG "\n****************************\n";
print LOG "Motifs : $Msur motifs read \n";
print LOG "Analyze is running...\n";
print LOG "$nbSeq sequences read \n";


###########################################################################################################################################################

##Création des fichiers motifs intermédiaires
#Récupération du nombre de motif total
my $nbMotifs = $Msur;

#On divise ce nombre par le nombre de threads souhaités par l'utilisateur
my $nbMotifsPerFile = int($nbMotifs/$thread);

#Création des fichiers motifs
my $fMotifTemp = $fMotif."Temp";
system("split -d -l ".$nbMotifsPerFile." ".$fMotif." ".$fMotifTemp);

#Préparation de la ligne de commande pour les lancements en parallèle
my @commande;
my @subrepo;

for(my $i = 0; $i <= $thread; $i++){
	#Lorsque le numéro du fichier est inférieur à 10, un "0" est ajouté devant, il faut donc le prendre en compte
	if($i < 10){
		my $fMotifTemp0 = $fMotifTemp."0";
		if(-f $fMotifTemp0.$i){
			push(@commande,"perl ".dirname($0)."/PLMdetect_threads.pl --seqFile $fSeq --motifFile ".$fMotifTemp0.$i." --dir ".$dir.$i." --alpha $alpha --strand $brin --scoreIC $seuilIC --replaceDir $replaceDir --slidingWindow $lissage --format $format --round $arrondi --UTR $UTR --annotation $BD --smooth $doublelissage --publi $publi --LA1 $LA1 --LA2 $LA2");
		}
	#Lorsqu'il est supérieur à 10, les indices restent corrects et il n'y a pas besoin d'ajouter de 0 dans le nom du fichier motif à traiter
	}else{
		if(-f $fMotifTemp.$i){
			push(@commande,"perl ".dirname($0)."/PLMdetect_threads.pl --seqFile $fSeq --motifFile ".$fMotifTemp.$i." --dir ".$dir.$i." --alpha $alpha --strand $brin --scoreIC $seuilIC --replaceDir $replaceDir --slidingWindow $lissage --format $format --round $arrondi --UTR $UTR --annotation $BD --smooth $doublelissage --publi $publi --LA1 $LA1 --LA2 $LA2");
		}
	}

	#On stocke dans un tableau l'ensemble des répertoires de résultats pour pouvoir les réconcilier à posteriori
	push(@subrepo,$dir.$i);	

}


#Exécution parallèle des processus
my $pm = new Parallel::ForkManager($thread);

foreach my $k (@commande) {
	#Fork
	my $pid = $pm->start and next;

	#Traitement
	my $valpg = system($k);
	if($valpg != 0){print "*** Error: $valpg $!***\n"; print LOG "*** Error: $valpg $!***\n"; exit;}
	else{print '.';print LOG '.';}

	# Fin du traitement
	$pm->finish;
}

# Attente de tous les sous-processus avant de continuer
$pm->wait_all_children;
print "\ndone\n";
print LOG "\ndone\n";

#Suppression des fichiers intermédiaires
system("rm ".$fMotifTemp."*");

###########################################################################################################################################################

#Les processus ayant tournés en parralèle, il me faut regrouper les résultats dans un répertoire bilan, je créé ce répertoire selon les anciennes procédures
#Recréation de l'architecture des dossiers actuels
mkdir($dir."/MotifsDInteret/");
mkdir($dir."/MotifsDInteret/surRepresente/");
mkdir($dir."/MotifsDInteret/surRepresente/ListeMotifsInteret/");
mkdir($dir."/Elimines_Intermediaires/");
mkdir($dir."/Elimines_Intermediaires/surRepresente/");
mkdir($dir."/Elimines_Intermediaires/surRepresente/MaxiDansRef/");
mkdir($dir."/Elimines_Intermediaires/surRepresente/MaxiSousSeuil/");
mkdir($dir."/PLM_graphs/");
mkdir($dir."/PLM_lists/");

#On récupère et déplace pour chaque sous répertoires les fichiers et informations nécessaires

#Table stockant l'ensemble des lignes des différents fichiers bilans
my %Bilan_complet;
my %Bilan_score;

#Parcours des dossiers intermédiaires
foreach my $repo (@subrepo){
	#Lors des déplacements suivants l'option 2>/dev/null sert à ne pas afficher les erreurs qui ici correspondent à l'absence de motif detectés dans certains sous repo
	#1.Déplacement des graphs PLM_graphs
	system("mv ".$repo."/PLM_graphs/* ".$dir."/PLM_graphs/ 2>/dev/null");

	#2.Déplacement PLM_lists
	system("mv ".$repo."/PLM_lists/* ".$dir."/PLM_lists/ 2>/dev/null");
	
	#3.Déplacement des listes non PLM 
	system("mv ".$repo."/MotifsDInteret/surRepresente/ListeMotifsInteret/* ".$dir."/MotifsDInteret/surRepresente/ListeMotifsInteret/ 2>/dev/null");

	#4.Déplacement graphs non PLM
	system("mv ".$repo."/MotifsDInteret/surRepresente/* ".$dir."/MotifsDInteret/surRepresente/ 2>/dev/null");
	system("mv ".$repo."/Elimines_Intermediaires/surRepresente/MaxiSousSeuil/* ".$dir."/Elimines_Intermediaires/surRepresente/MaxiSousSeuil/ 2>/dev/null");
	system("mv ".$repo."/Elimines_Intermediaires/surRepresente/MaxiDansRef/* ".$dir."/Elimines_Intermediaires/surRepresente/MaxiDansRef/ 2>/dev/null");

	#5.Récupération des informations des fichiers Bilan
	open(BILAN_TEMP,$repo."/BilanSEMMsup1.recup_ALL");
	while(<BILAN_TEMP>){
		chomp($_);
		if(!($_ =~ m/^motif/)){
			my @ligne_bilan = split(/\t/,$_);
			my $motif = $ligne_bilan[0];
			my $score = $ligne_bilan[2];
			$Bilan_score{$motif} = $score;
			$Bilan_complet{$motif} = $_;
		}
	}
	close(BILAN_TEMP);
}


#Ecriture du bilan complet
my $bilan_final = $dir."/BilanSEMMsup1.recup_ALL";
open(BILAN, ">$bilan_final");
print BILAN "motif	winUsed	ScoreSMS	PP	functionalWin	nbGenes	%	nbPromos	sizeFuncWin	function	is_core	family_place	description	biblio	organisms\n";

foreach my $k (sort ({$Bilan_score{$b} <=> $Bilan_score{$a} } keys %Bilan_score)){
	print BILAN "$Bilan_complet{$k}\n";
}

close(BILAN);

#Ecriture de la matrice PLMs - gènes
my $valpg=system(dirname($0)."/LIB_PERL_R/Matrice_threads.pl  -dir $dir");
if($valpg != 0 ){print "*** Error : $valpg $!***\n";print LOG "*** Error : $valpg $!***\n";}

#Realisation des tests d'enrichissement si un fichier de référence est fourni

my $valpg=system(dirname($0)."/LIB_PERL_R/Enrichissement.pl -refFile $refFile -bilan $bilan_final -dir $dir");
if($valpg != 0 ){print "*** Error : $valpg $!***\n";print LOG "*** Error : $valpg $!***\n";}

#Ecriture dans le log
# comptage nbr de PLM trouves
print LOG "\n### RESULTS ###\n\n";
print "\n### RESULTS ###\n\n";

my $nbPLM=`wc -l $bilan_final`;
print LOG "PLM found : ".($nbPLM -1)."\n";	
print  "PLM found : ".($nbPLM -1)."\n";

print LOG "\n  --> Find all the PLM with SMS > $seuilIC peak in \n$dir/BilanSEMMsup1.recup_ALL";
print "\n  --> Find all the PLM with SMS > $seuilIC peak in \n$dir/BilanSEMMsup1.recup_ALL";
print LOG "\n  --> All the gene lists in directory $dir"."/MotifsDInteret/surRepresente/ListeMotifsInteret/*.ListeGenes";
print "\n  --> All the gene lists in directory $dir"."/MotifsDInteret/surRepresente/ListeMotifsInteret/*.ListeGenes";
print LOG "\n      listing of gene containing motifs in their functional windows";
print "\n      listing of gene containing motifs in their functional windows";
print LOG "\n  --> And figures in directory $dir"."/MotifsDInteret/surRepresente/*.bmp or *.pdf\n\n";
print "\n  --> And figures in directory $dir"."/MotifsDInteret/surRepresente/*.bmp or *.pdf\n\n";

print LOG "\n ##Analysis informations :\n";

#Informations sur le traitement effectué
print LOG "\n  --> Date : $Date";
print LOG "\n  --> Sequences file used for analysis : $fSeq";
print LOG "\n  --> Motifs file used for analysis : $fMotif\n";
print LOG "\n  --> Graphics and gene lists foreach PLM are available in PLM_graphs and PLM_lists\n";

#Résumé des paramètres utilisés (à garder à chaque nouveau lancement pour réaliser des comparaisons futures)
print LOG "\n  --> Parameters values - scoreIC : $seuilIC, strand(s) : $brin, alpha : $alpha, PP lower limit : $LimInfPP, PP upper limit : $CHEMIN::LimSupPP, Min FF size : $CHEMIN::LimInfL, Max FF size : $CHEMIN::LimSupL, Minimum number of sequences : $CHEMIN::NbSeq  \n";

#Ajout des informations sur le lissage (fenêtre glissante)	
if($lissage eq ""){
	print LOG "\n  --> Size of the sliding window : default (increasing size)\n\n";
}else{
	print LOG "\n  --> Size of the sliding window : $lissage\n\n";
}
	
print LOG "\n PLMs are sought between $LimInfPP and $CHEMIN::LimSupPP\n\n";
	

close(LOG);

#Suppression de l'ensemble des analyses intermédiaires
for(my $i = 0; $i <= $thread; $i++){
	if(-d $dir.$i){
		system("rm -r ".$dir.$i);
	}
}

#Suppression du Bilan intermédiaire
#system("rm ".$bilan_final);
